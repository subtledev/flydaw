#![cfg_attr(
    all(not(debug_assertions), target_os = "windows"),
    windows_subsystem = "windows"
)]


use tauri::Manager;

use tauri::{CustomMenuItem, Menu, MenuItem, Submenu};

use tokio::io::{AsyncBufReadExt, BufReader};
use tokio::sync::mpsc;
use tokio::sync::Mutex;
use tracing::info;
use tracing_subscriber;


use tauri::api::dialog::FileDialogBuilder;


struct AsyncProcInputTx {
    inner: Mutex<mpsc::Sender<String>>,
}


fn main() {
    tracing_subscriber::fmt::init();

    let (async_proc_input_tx, async_proc_input_rx) = mpsc::channel(1);
    let (async_proc_output_tx, _async_proc_output_rx) = mpsc::channel(1);


    let async_proc_input_tx_clone = async_proc_input_tx.clone();

    // TODO: might wanna move menu creation to a structure on a separate file.
    let file_submenu = Submenu::new(
        "File",
        Menu::new()
            .add_item(CustomMenuItem::new("save".to_string(), "Save"))
            .add_item(CustomMenuItem::new("load".to_string(), "Load"))
            .add_item(CustomMenuItem::new("close".to_string(), "Close"))
    );
    let menu = Menu::new()
      .add_native_item(MenuItem::Copy)
      .add_submenu(file_submenu);

    let state = AsyncProcInputTx {
        inner: Mutex::new(async_proc_input_tx.clone()),
    };


    tauri::Builder::default()
        .menu(menu)
        .manage(state)
        .invoke_handler(tauri::generate_handler![js2rs])
        .setup(|app| {
            let window = app.get_window("main").unwrap();

            #[cfg(debug_assertions)]
            {
                window.open_devtools();
            }

            let window_clone = window.clone();
            window.on_menu_event(move |event| {
                match event.menu_item_id() {
                    "save" => {
                        let input_tx_clone = async_proc_input_tx.clone();
                        FileDialogBuilder::new()
                            .set_title("Save FlyDaw session")
                            .add_filter("FlyDaw Session files", &["fdaw", "fdaw.yaml"])
                            .set_directory("/home/qualphey/FlyDaw")
                            .save_file(move |file_path| {
                                let command_str = format!(
                                    "{{\"command\": \"session\", \"action\": \"save_as\", \"full_path\": \"{}\"}}",
                                    file_path.unwrap().display()
                                );
                                println!("{}", command_str);
                                input_tx_clone.send(command_str);
                            });
                    }
                    "load" => {
                        let input_tx_clone = async_proc_input_tx.clone();
                        FileDialogBuilder::new()
                            .set_title("Load FlyDaw session")
                            .add_filter("FlyDaw Session files", &["fdaw", "fdaw.yaml"])
                            .set_directory("/home/qualphey/FlyDaw")
                            .pick_file(move |file_path| {
                                let command_str = format!(
                                    "{{\"command\": \"session\", \"action\": \"load\", \"full_path\": \"{}\"}}",
                                    file_path.unwrap().display()
                                );
                                println!("{}", command_str);
                                input_tx_clone.send(command_str);
                            });
                    }
                    "close" => {
                        window_clone.close().unwrap();
                    }
                    _ => {}
                }
            });

            tauri::async_runtime::spawn(async move {
                async_process_model(
                    async_proc_input_rx,
                    async_proc_output_tx,
                ).await
            });

            let app_handle = app.handle();
            tauri::async_runtime::spawn(async move {


                let stdin = tokio::io::stdin();
                let mut reader = BufReader::new(stdin);

                loop {
                    let mut line = String::new();
                    if let Ok(len) = reader.read_line(&mut line).await {
                        if len > 0 {
                            rs2js(line.to_string(), &app_handle);
                        }
                    }
                }
            });

            Ok(())
        })
        .run(tauri::generate_context!())
        .expect("error while running tauri application");
}

fn rs2js<R: tauri::Runtime>(message: String, manager: &impl Manager<R>) {
    //println!("rs2js {}", message);
    manager
        .emit_all("rs2js", message)
        .unwrap();
}

#[tauri::command]
async fn js2rs(
    message: String,
    state: tauri::State<'_, AsyncProcInputTx>,
) -> Result<(), String> {
    println!("{}", message);
    let async_proc_input_tx = state.inner.lock().await;
    async_proc_input_tx
        .send(message)
        .await
        .map_err(|e| e.to_string())
}


async fn async_process_model(
    mut input_rx: mpsc::Receiver<String>,
    output_tx: mpsc::Sender<String>,
) -> Result<(), Box<dyn std::error::Error + Send + Sync>> {
    while let Some(input) = input_rx.recv().await {
        let output = input;
        output_tx.send(output).await?;
    }

    Ok(())
}
