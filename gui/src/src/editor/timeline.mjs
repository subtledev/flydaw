
const track_bodies = document.getElementById("track-bodies");
const timeline_div = document.getElementById("timeline");

export default class Timeline {
  constructor(editor) {
    this.session = editor.session;

    this.canvas = document.createElement("canvas");
    this.canvas.id = "timeline-canvas";
    this.canvas.height = timeline_div.offsetHeight;

    timeline_div.appendChild(this.canvas);

    this.render();
  }

  render() {
    let session = this.session;


    const step = Math.ceil(60 / session.px_per_sec);
    const step_pixels = step * session.px_per_sec;

    const total_steps = Math.floor(session.duration/session.sample_rate);

    
    this.canvas.width = track_bodies.offsetWidth;
    this.canvas.height = timeline_div.offsetHeight;

    this.canvas.style.marginLeft = track_bodies.scrollLeft + "px";

    let steps_to_render = Math.ceil(this.canvas.width / step_pixels) + 1;

    let starting_position = track_bodies.scrollLeft == 0 ? 0 :
      step_pixels - (track_bodies.scrollLeft % step_pixels);

    let steps_behind = Math.ceil(track_bodies.scrollLeft / step_pixels);

    const ctx = this.canvas.getContext("2d");
    ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);

    for (let t = 0; t < steps_to_render; t++) {
      let x = t * step_pixels + starting_position;

      ctx.strokeStyle = "#0F0";
      ctx.beginPath();
      ctx.moveTo(x, 0);
      ctx.lineTo(x, this.canvas.height);
      ctx.stroke();

      ctx.strokeStyle = "#0F0";
      ctx.font = "12px Arial";

      let secs = steps_behind+t*step;
      let mins = Math.floor(secs / 60);
      let hours = Math.floor(mins / 60);

      secs = (secs % 60).toString();
      if (secs.length < 2) secs = "0"+secs;
      mins = (mins % 60).toString();
      if (mins.length < 2) mins = "0"+mins;
      hours = hours.toString();
      if (hours.length < 2) hours = "0"+hours;

      ctx.fillText(`${hours}:${mins}:${secs}`, x+2, 36);
    }

  }
}
