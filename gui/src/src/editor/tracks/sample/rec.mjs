
import { invoke } from '@tauri-apps/api/tauri'

export default class SampleRec {
  constructor(editor, track, id, full_path, position) {
    const _this = this;

    this.editor = editor;
    this.session = editor.session;

    this.track = track;
    this.id = id;
    this.full_path = full_path;
    this.position = position || 0;
    this.duration = this.editor.transport.current_position - this.position;


    this.body = document.createElement('div');
    this.body.innerHTML = full_path;
    this.body.classList.add("sample-rec");

    track.body.appendChild(this.body);
    this.update();
  }

  update() {
    this.body.style.left = this.position * this.session.px_per_sec + "px";
    this.duration = this.editor.transport.current_position - this.position;
    this.body.style.width = this.duration * this.session.px_per_sec + "px";
  }

  destroy() {
    this.body.parentElement.removeChild(this.body);
  }
  
}
