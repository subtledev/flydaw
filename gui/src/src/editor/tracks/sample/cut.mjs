
import { invoke } from '@tauri-apps/api/tauri'

import cut_html from './cut.html'

function createElementFromHTML(htmlString) {
  var container = document.createElement('div');
  container.innerHTML = htmlString;
  return container.firstChild;
}

export default class SampleCut {
  constructor(editor, track, id, full_path, duration, position) {
    const _this = this;

    this.editor = editor;
    this.session = editor.session;

    this.track = track;
    this.id = id;
    this.full_path = full_path;
    this.duration = duration;
    this.position = position || 0;

    this.start = 0;
    this.end = duration;

    this.body = createElementFromHTML(cut_html);
    this.body.classList.add("sample-cut");

    this.path_block = this.body.querySelector("div.cut-path");
    this.path_block.innerHTML = full_path;


    this.w_resize = this.body.querySelector("div.resize-left");
    this.w_resize_listener = (e) => {
      console.log("W_DOWN");
      _this.w_down = true;
    };
    this.w_resize.addEventListener("mousedown", this.w_resize_listener);

    this.e_resize = this.body.querySelector("div.resize-right");
    this.e_resize_listener = (e) => {
      _this.e_down = true;
    }
    this.e_resize.addEventListener("mousedown", this.e_resize_listener);


    this.grabbed = false;

    this.path_block.addEventListener("mousedown", (e) => {
      _this.grabbed = true;
    });

    this.body.addEventListener("contextmenu", (e) => {
      e.preventDefault();
      editor.tracks.scut_ctxm.open(e, (entry) => {
        switch (entry) {
          case "remove":
            invoke('js2rs', {
              message: JSON.stringify({
                command: "cut",
                action: "remove",
                track: _this.track.name,
                id: _this.id
              })
            })

            break;
          default:
        }
      });
    });

    window.addEventListener("mouseup", (e) => {
      if (_this.grabbed) {
        _this.grabbed = false;
        prev_x = undefined;

        invoke('js2rs', {
          message: JSON.stringify({
            command: "cut",
            action: "set_position",
            track: _this.track.name,
            position: _this.position,
            cut_id: _this.id
          })
        })
      } else if (_this.w_down) {
        _this.w_down = false;
        prev_x = undefined;
      } else if (_this.e_down) {
        _this.e_down = false;
      }
    });

    let prev_x = undefined;
    window.addEventListener("mousemove", (e) => {
      if (_this.grabbed) {
        const cur_x = e.screenX;

        let delta = 0;
        if (typeof prev_x !== "undefined") delta = cur_x - prev_x;

        prev_x = cur_x;

        _this.position += delta / _this.session.px_per_sec;
        if (_this.position < 0) _this.position = 0; 

        for (let otrack of _this.editor.tracks.list) {
          if (otrack.mouseover && otrack.name !== _this.track.name) {
            _this.track.sample_cuts.splice(_this.track.sample_cuts.indexOf(_this), 1);
            _this.track.body.removeChild(_this.body);

            _this.track = otrack;

            otrack.sample_cuts.push(_this);
            _this.track.body.appendChild(_this.body);
          }
        }

        _this.update();
      } else if (_this.w_down) {
        const cur_x = e.screenX;
        let delta = 0;
        if (typeof prev_x !== "undefined") delta = cur_x - prev_x;
        prev_x = cur_x;

        let distance = delta / _this.session.px_per_sec;
        _this.start += distance;

        if (_this.start < 0) {
          distance += _this.start;
          _this.start = 0;
        }

        console.log("distance", distance);

        _this.position += distance;
        _this.duration -= distance;
        if (_this.position < 0) {
          let add_to_start = _this.position * -1;
          _this.position = 0;
          _this.start += add_to_start;
        }

        _this.update();
      } else if (_this.e_down) {
        const cur_x = e.screenX;
        let delta = 0;
        if (typeof prev_x !== "undefined") delta = cur_x - prev_x;
        prev_x = cur_x;
        _this.end += delta / _this.session.px_per_sec;
        if (_this.duration < _this.end) _this.end = _this.duration;


        _this.update();
      }
    });

    track.body.appendChild(this.body);
    this.update();
  }

  update() {
    this.body.style.left = this.position * this.session.px_per_sec + "px";
    this.body.style.width = this.duration * this.session.px_per_sec + "px";
  }
  
}
