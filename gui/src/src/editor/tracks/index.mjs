
import Track from "./track.mjs"


import ContextMenu from "../ctx/menu.mjs"

const track_heads_div = document.getElementById("track-heads");
const track_bodies_div = document.getElementById("track-bodies");

export default class Tracks {
  constructor(editor) {
    this.editor = editor;
    this.list = [];

    let ctrl = false;
    window.addEventListener("keydown", (e) => {
      if (e.key == "Control") ctrl = true;
    });
    window.addEventListener("keyup", (e) => {
      if (e.key == "Control") ctrl = false;
    });

    const track_ctxm = this.track_ctxm = new ContextMenu([
      {
        text: "Remove",
        label: "remove"
      }
    ]);

    const scut_ctxm = this.scut_ctxm = new ContextMenu([
      {
        text: "Remove",
        label: "remove"
      }
    ]);


    track_bodies_div.addEventListener("wheel", (e) => {
      if (ctrl) {
        e.preventDefault();
        editor.zoom(Math.round(e.wheelDeltaY/10));
      }
    });

  }

  select(t_name) {
    for (let track of this.list) {
      if (track.name == t_name) return track;
    }
  }

  remove(t_name) {
      const track = this.select(t_name);
      track_heads_div.removeChild(track.head);
      track_bodies_div.removeChild(track.body);

      for (let i = this.list.length - 1; i >= 0; i--) {
        if (this.list[i].name === name) {
          this.list.splice(i, 1);
        }
      }
  }

  update() {
    for (let track of this.list) {
      track.update();
    }
  }

  record() {
    for (let track of this.list) {
      track.update_sample_recs();
    }
  }

  unload() {
    for (let track of this.list) {
      track.remove();
    }
    this.list = [];
  }

  cmd(data) {
    if (data.new_track) {
      this.list.push(new Track(
        this.editor,
        data.new_track.type,
        data.new_track.name
      ));
    } else if (data.add_sample_cut) {
      const track = this.select(data.add_sample_cut.track);
      track.add_sample_cut(data.add_sample_cut);
    } else if (data.add_sequence) {
      const track = this.select(data.add_sequence.track);
      track.add_sample_cut(data.add_sequence);
    } else if (data.set_state) {
      console.log("set state", data.set_state);
      const track = this.select(data.set_state.track);
      track.set_state(data.set_state.which, data.set_state.state);
    } else if (data.recording) {
      const track = this.select(data.recording.track);
      if (data.recording.sample) {
        track.start_recording(data.recording.sample);
      } else if (data.recording.sequence) {
        track.start_recording(data.recording.sequence);
      }
    } else if (data.stop_recording) {
      const track = this.select(data.stop_recording.track);
      if (data.recording.sample) {
        track.stop_recording(data.stop_recording.sample);
      } else if (data.recording.sequence) {
        track.stop_recording(data.stop_recording.sequence);
      }
    } else if (data.remove) {
      this.remove(data.remove.name);
    }
  }

  cut_cmd(data) {
    if (data.remove) {
      this.select(data.remove.track).remove_sample_cut(data.remove.id);
    } else if (data.move) {
    }
  }
}
