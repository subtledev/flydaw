
import { invoke } from '@tauri-apps/api/tauri'

import track_head from './head.html'
import track_body from './body.html'

import SampleCut from './sample/cut.mjs'
import SampleRec from './sample/rec.mjs'

const track_heads_div = document.getElementById("track-heads");
const track_bodies_div = document.getElementById("track-bodies");


function createElementFromHTML(htmlString) {
  var container = document.createElement('div');
  container.innerHTML = htmlString;
  return container.firstChild;
}

export default class Track {

  constructor(editor, type, name) {
    const _this = this;

    this.editor = editor;
    this.name = name;

    this.cuts = [];
    this.sample_recs = [];

    this.head = createElementFromHTML(track_head);
    this.head.querySelector("h3").innerText = name;
    this.head.classList.add(type);

    this.head.addEventListener("contextmenu", (e) => {
      e.preventDefault();
      editor.tracks.track_ctxm.open(e, (entry) => {
        switch (entry) {
          case "remove":
            invoke('js2rs', {
              message: JSON.stringify({
                command: "mixer",
                action: "remove_track",
                name: _this.name,
                type: type
              })
            })
            break;
          default:
        }
      });
    });

    this.ctl_states = {
      mute: false,
      solo: false,
      record: false
    }


    this.track_opts = this.head.querySelector("div.track-options");

    this.opt_divs = {
      mute: this.track_opts.querySelector("div.mute"),
      solo: this.track_opts.querySelector("div.solo"),
      record: this.track_opts.querySelector("div.record")
    }

    for (let which_state in this.opt_divs) {
      this.opt_divs[which_state].addEventListener("mousedown", function() {
        if (_this.ctl_states[which_state]) {
          invoke('js2rs', {
            message: JSON.stringify({
              command: "mixer",
              action: "set_track_state",
              track_name: name,
              which: which_state,
              state: false
            })
          })
        } else {
          invoke('js2rs', {
            message: JSON.stringify({
              command: "mixer",
              action: "set_track_state",
              track_name: name,
              which: which_state,
              state: true
            })
          })
        }
      });

    }

    track_heads_div.insertBefore(this.head, track_heads_div.lastElementChild);

    this.body = createElementFromHTML(track_body);
    this.body.classList.add(type);

    this.mouseover = false;
    this.body.addEventListener("mouseenter", (e) => {
      _this.mouseover = true;
    });

    this.body.addEventListener("mouseleave", (e) => {
      _this.mouseover = false;
    });

    track_bodies_div.appendChild(this.body);

  }

  set_state(which, state) {
    if (state) {
      this.ctl_states[which] = true;
      this.opt_divs[which].classList.add("active");
    } else {
      this.ctl_states[which] = false;
      this.opt_divs[which].classList.remove("active");
    }
  }

  add_sample_cut(data) {
    this.cuts.push(new SampleCut(this.editor, this, data.id, data.full_path, data.duration, data.position));
  }

  remove_sample_cut(id) {
    console.log("sample cut", id);
    for (let i = this.cuts.length - 1; i >= 0; i--) {
      if (this.cuts[i].id === id) {
        this.body.removeChild(this.cuts[i].body);
        this.cuts.splice(i, 1);
      }
    }
  }

  import_files(files) {
    console.log("TRACK NAME", this.name);
    invoke('js2rs', {
      message: JSON.stringify({
        command: "mixer",
        action: "import_files",
        track: this.name,
        files: files
      })
    })
  }

  start_recording(data) {
    this.sample_recs.push(new SampleRec(this.editor, this, data.id, data.full_path, data.position));
  }

  stop_recording(data) {
    let duration = data.position - this.sample_recs[0].position;
    this.cuts.push(new SampleCut(this.editor, this, this.sample_recs[0].id, this.sample_recs[0].full_path, duration, this.sample_recs[0].position));
    this.sample_recs[0].destroy();
    this.sample_recs = [];
  }

  update() {
    for (let sample_cut of this.cuts) {
      sample_cut.update();
    }
  }

  update_sample_recs() {
    for (let sample_rec of this.sample_recs) {
      sample_rec.update();
    }
  }

  remove() {
    track_heads_div.removeChild(this.head);
    track_bodies_div.removeChild(this.body);
  }

}
