

import body_html from "./body.html";


const track_bodies = document.getElementById("track-bodies");

function createElementFromHTML(htmlString) {
  var container = document.createElement('div');
  container.innerHTML = htmlString;
  return container.firstChild;
}

export default class SampleRegion {

  constructor(session, class_name) {
    const _this = this;
    this.session = session;

    this.element = createElementFromHTML(body_html);
    this.element.classList.add(class_name);

    this.w_resize = this.element.querySelector("div.resize-left");
    this.w_resize_listener = (e) => {
      _this.w_down = true;
    };
    this.w_resize.addEventListener("mousedown", this.w_resize_listener);

    this.e_resize = this.element.querySelector("div.resize-right");
    this.e_resize_listener = (e) => {
      _this.e_down = true;
    }
    this.e_resize.addEventListener("mousedown", this.e_resize_listener);


    this.mouseup_listener = (e) => {
      if (_this.mousedown) {
        _this.mousedown = false;
/*
        _this.selected_pixels.start = _this.samples_to_pixels(_this.selected_samples.start);
        _this.selected_pixels.end = _this.samples_to_pixels(_this.selected_samples.end);
*/
        _this.selected_samples = {
          start: _this.pixels_to_samples(_this.selected_pixels.start),
          end: _this.pixels_to_samples(_this.selected_pixels.end)
        }

        
        _this.element.style.left = _this.selected_pixels.start+"px";
        let selection_width = _this.selected_pixels.end - _this.selected_pixels.start;
        _this.element.style.width = selection_width+"px";
      } else if (_this.w_down) {
        _this.w_down = false;

        _this.selected_samples = {
          start: _this.pixels_to_samples(_this.selected_pixels.start),
          end: _this.pixels_to_samples(_this.selected_pixels.end)
        }
      } else if (_this.e_down) {
        _this.e_down = false;

        _this.selected_samples = {
          start: _this.pixels_to_samples(_this.selected_pixels.start),
          end: _this.pixels_to_samples(_this.selected_pixels.end)
        }
      }
    }
    window.addEventListener("mouseup", this.mouseup_listener);

    let prev_x = undefined;

    this.mousemove_listener = (e) => {
      if (_this.mousedown) {
        const t_x = e.screenX - track_bodies.offsetLeft + track_bodies.scrollLeft;
        console.log("layer x", t_x);
        if (t_x <= _this.selected_pixels.start) {
          _this.selected_pixels.start = t_x < 0 ? 0 : t_x;
        } else {
          _this.selected_pixels.end = t_x;
        }
        
        _this.element.style.left = _this.selected_pixels.start+"px";
        let selection_width = _this.selected_pixels.end - _this.selected_pixels.start;
        _this.element.style.width = selection_width+"px";
      } else if (_this.w_down) {
        const t_x = e.screenX - track_bodies.offsetLeft + track_bodies.scrollLeft;

        if (t_x < _this.selected_pixels.end - 6) {
          _this.selected_pixels.start = t_x < 0 ? 0 : t_x;
        }

        _this.element.style.left = _this.selected_pixels.start+"px";
        let selection_width = _this.selected_pixels.end - _this.selected_pixels.start;
        _this.element.style.width = selection_width+"px";
      } else if (_this.e_down) {
        const t_x = e.screenX - track_bodies.offsetLeft + track_bodies.scrollLeft;

        if (_this.selected_pixels.start + 6 < t_x) {
          _this.selected_pixels.end = t_x;
        }

        let selection_width = _this.selected_pixels.end - _this.selected_pixels.start;
        _this.element.style.width = selection_width+"px";
      }
    };
    window.addEventListener("mousemove", this.mousemove_listener);
  }

  pixels_to_samples(px) {
    return Math.floor(
      px / this.session.px_per_sec * this.session.sample_rate / this.session.frame_size
    ) * this.session.frame_size;
  }

  samples_to_pixels(samples) {
    return samples / this.session.sample_rate * this.session.px_per_sec;
  }

  update() {
    if (this.selected_samples) {
      this.selected_pixels.start = this.samples_to_pixels(this.selected_samples.start);
      this.selected_pixels.end = this.samples_to_pixels(this.selected_samples.end);

          
      this.element.style.left = this.selected_pixels.start+"px";
      let selection_width = this.selected_pixels.end - this.selected_pixels.start;
      this.element.style.width = selection_width+"px";
    }
  }

  deselect() {
    this.selected_samples = undefined;
  }
}

