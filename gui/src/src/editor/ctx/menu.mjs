

export default class ContextMenu {
  constructor(entries) {
    let _this = this;

    this.element = document.createElement("div");
    this.element.classList.add("ctx-menu");

    for (let entry of entries) {
      let ee = document.createElement("div");
      ee.innerText = entry.text;

      ee.addEventListener("click", (e) => {
        if (typeof _this.cur_cb == "function") {
          _this.element.style.display = "";
          _this.cur_cb(entry.label);
          _this.cur_cb = undefined;
        }
      });

      this.element.appendChild(ee);
    }

    document.body.appendChild(this.element);
  }

  open(e, cb) {
    this.element.style.display = "block";
    this.element.style.left = e.screenX+"px";
    this.element.style.top = e.screenY+"px";

    this.cur_cb = cb;
  }
}
