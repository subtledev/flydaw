
import { invoke } from '@tauri-apps/api/tauri'

const metronome_div = document.getElementById("metronome");

const numerator_div = document.getElementById("ts-numerator");
const denominator_div = document.getElementById("ts-denominator");

const bpm_div = document.getElementById("bpm-ctl");

export default class Session {
  constructor(editor) {
    this.editor = editor;
    this.sample_rate = 48000;
    this.duration = this.sample_rate * 60 * 5;
    this.frame_size = 128;
    this.px_per_sec = 100;
    this.metronome_on = false;

    // TODO: these default values ain't even necessary here. Should be set by session command.
    this.bpm = 120;
    this.time_signature = {
      numerator: 4,
      denominator: 4
    }


    let _this = this;
    metronome_div.addEventListener("click", () => {
      console.log(`current state ${_this.metronome_on}`);
      let new_state = !_this.metronome_on;
      console.log(`set metronome`, {
          command: "session",
          action: "metronome",
          state: new_state
        });
      invoke('js2rs', {
        message: JSON.stringify({
          command: "session",
          action: "metronome",
          state: new_state
        })
      })
    });

    function time_signature_cb(e) {
      invoke('js2rs', {
        message: JSON.stringify({
          command: "session",
          action: "time_signature",
          numerator: parseInt(numerator_div.value),
          denominator: parseInt(denominator_div.value)
        })
      })
    }

    numerator_div.addEventListener("input", time_signature_cb);
    denominator_div.addEventListener("input", time_signature_cb);

    bpm_div.addEventListener("input", (e) => {
      invoke('js2rs', {
        message: JSON.stringify({
          command: "session",
          action: "bpm",
          value: parseFloat(bpm_div.value)
        })
      })
    });
  }


  cmd(data) {
    if (data.metronome) {
      this.metronome_on = data.metronome.state;
    } else if (data.time_signature) {
      numerator_div.value = this.time_signature.numerator = data.time_signature.numerator;
      denominator_div.value = this.time_signature.denominator = data.time_signature.denominator;
      this.editor.zoom();
    } else if (data.bpm) {
      bpm_div.value = this.bpm = data.bpm;
      this.editor.zoom();
    } else if (data.unload) {
      this.editor.unload();
    }
  }

  pixels_to_samples(px) {
    return Math.floor(
      px / this.px_per_sec * this.sample_rate / this.frame_size
    ) * this.frame_size;
  }

  samples_to_pixels(samples) {
    return samples / this.sample_rate * this.px_per_sec;
  }
}
