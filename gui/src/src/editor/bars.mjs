
const track_bodies = document.getElementById("track-bodies");
const bars_div = document.getElementById("bars");

export default class Bars {
  constructor(editor) {
    this.session = editor.session;
    this.editor = editor;

//    this.canvases = [];
    this.canvas = document.createElement("canvas");
    this.canvas.id = "bars-canvas";
    this.canvas.height = 100;
    bars_div.appendChild(this.canvas);


    this.render();

    let _this = this;

  }

  render() {
    let session = this.session;
    let { numerator, denominator } = session.time_signature;

    let tick_samples = 60 / session.bpm * 4 / denominator * session.sample_rate;
    let bar_samples = tick_samples * numerator;

    let tick_pixels = session.samples_to_pixels(tick_samples);

    let total_ticks = Math.floor(session.duration/tick_samples);

    let whole_width = tick_pixels * total_ticks;

    this.canvas.width = track_bodies.offsetWidth;
    this.canvas.height = bars_div.offsetHeight-10;

    this.canvas.style.marginLeft = track_bodies.scrollLeft + "px";


    let ticks_to_render = Math.ceil(this.canvas.width / tick_pixels) + 1;

    let starting_position = track_bodies.scrollLeft == 0 ? 0 :
      tick_pixels - (track_bodies.scrollLeft % tick_pixels);

    let ticks_behind = Math.ceil(track_bodies.scrollLeft / tick_pixels);
    let current_tick = ticks_behind % numerator;


    const ctx = this.canvas.getContext("2d");
    ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);

    for (let t = 0; t < ticks_to_render; t++) {
      if (current_tick == 1) {
        ctx.strokeStyle = "#F00";
      } else {
        ctx.strokeStyle = "#00F";
      }

      let x = t * tick_pixels + starting_position - tick_pixels;

      ctx.beginPath();
      ctx.moveTo(x, 0);
      ctx.lineTo(x, this.canvas.height);
      ctx.stroke();

      if (current_tick == numerator-1) {
        current_tick = 0
      } else {
        current_tick++;
      }
    }

  }
}
