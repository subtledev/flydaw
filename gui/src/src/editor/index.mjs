
import { invoke } from '@tauri-apps/api/tauri'
import { listen } from '@tauri-apps/api/event'

import Tracks from "./tracks/index.mjs"
import Transport from "./transport.mjs"
import Session from "./session.mjs"


import Timeline from "./timeline.mjs"
import Bars from "./bars.mjs"


const track_bodies = document.getElementById("track-bodies");

export default class Editor {
  constructor() {
    const _this = this;


    this.session = new Session(this);
    this.tracks = new Tracks(this);

    this.timeline = new Timeline(this);
    this.bars = new Bars(this);

    this.transport = new Transport(this, this.selected_region, () => {
      _this.tracks.record();
    });
    


    listen('tauri://file-drop', event => {
      if (_this.tracks.list.length > 0) {
        _this.tracks.list[0].import_files(event.payload);
      }
    })

    invoke('js2rs', {
      message: JSON.stringify({
        command: "process",
        action: "ready"
      })
    });
/*    invoke('js2rs', {
      message: `{"command":"session","action":"load","full_path":"/home/qualphey/FlyDaw/test-session/session.fdaw.yaml"}`
    });*/
    


    let ctrl = false;
    window.addEventListener("keydown", (e) => {
      if (e.key == "Control") ctrl = true;
    });
    window.addEventListener("keyup", (e) => {
      if (e.key == "Control") ctrl = false;
    });
    
    track_bodies.addEventListener("scroll", (e) => {
      if (ctrl) {
        e.preventDefault();
      } else {
        _this.zoom();
      }
    });


  }

  unload() {
    this.tracks.unload();
  }

  zoom(val) {
    if (!val) val = 0;

    this.session.px_per_sec += val;
    if (this.session.px_per_sec < 1) {
      this.session.px_per_sec = 1;
    } else if (this.session.px_per_sec > 1000) {
      this.session.px_per_sec = 1000;
    }

    this.timeline.render();
    this.bars.render();


    this.transport.zoom();
    this.tracks.update();
  }

  cmd(data) {
    if (!data.command) {
      console.error(new Error("No command property in the object sent by parent!"));
      return;
    }

    switch (data.command) {
      case "tracks":
        this.tracks.cmd(data);
        break;
      case "transport":
        this.transport.cmd(data);
        break;
      case "edit":

        break;
      case "session":
        this.session.cmd(data);
        break;
      case "error":

        break;
      case "cut":
        this.tracks.cut_cmd(data);
        break;
      default:
        console.error(new Error("Invalid command property sent by parent!"));
    }
  }
}
