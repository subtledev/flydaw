
import { invoke } from '@tauri-apps/api/tauri'


import SampleRegion from "./region/index.mjs"

// 1 sec
// 48000 samples
// 48000 / 128 = 375 frames
// 1 pixel - 1 or 1/1000000 sample
// Every frame should be marked to give a visual representation of buffer size and sample rate.
// Sample is the smallest available unit. Smallest selected region is a frame to respect buffer size.

const play_pause_toggle = document.getElementById("tctl-play-pause");
const stop_button = document.getElementById("tctl-stop");
const loop_toggle = document.getElementById("tctl-loop");

const time_screen = document.getElementById("tmon-time");

const track_bodies = document.getElementById("track-bodies");

const timeline_div = document.getElementById("timeline");
const transport_div = document.getElementById("transport");

export default class Transport {

  constructor(editor, selected_region, up_cb) {
    const _this = this;

    this.editor = editor;

    this.selected_region = selected_region;
    
    this.session = editor.session;

    this.is_playing = false;

    this.position = {
      samples: 0,
      ms: 0
    }

    time_screen.innerText = this.time_string();


    this.current_position = 0;
    this.last_update_time = null;

    play_pause_toggle.addEventListener("mousedown", (e) => {
      if (_this.is_playing) {
        invoke('js2rs', {
          message: JSON.stringify({
            command: "transport",
            action: "pause"
          })
        })
      } else {
        invoke('js2rs', {
          message: JSON.stringify({
            command: "transport",
            action: "play"
          })
        })
      }
    });

    stop_button.addEventListener("mousedown", (e) => {
      invoke('js2rs', {
        message: JSON.stringify({
          command: "transport",
          action: "stop"
        })
      })
    });


    this.selected_region = new SampleRegion(this.session, "selection");

    timeline_div.addEventListener("mousedown", (e) => {
      const t_x = e.screenX - track_bodies.offsetLeft + track_bodies.scrollLeft;
      _this.selected_region.selected_pixels = { start: t_x, end: t_x+1 };
      _this.selected_region.element.style.left = t_x+"px";
      _this.selected_region.element.style.width = "1px";
      _this.selected_region.mousedown = true;

      if (!track_bodies.contains(_this.selected_region.element)) {
        track_bodies.appendChild(_this.selected_region.element);
      }
    });

    loop_toggle.addEventListener("mousedown", () => {
      if (_this.is_looping) {
        invoke('js2rs', {
          message: JSON.stringify({
            command: "transport",
            action: "end-loop"
          })
        })
      } else {
        
        if (_this.selected_region.selected_samples) {
          invoke('js2rs', {
            message: JSON.stringify({
              command: "transport",
              action: "loop",
              start: _this.selected_region.selected_samples.start,
              end: _this.selected_region.selected_samples.end
            })
          });
        } else if (_this.loop_region.selected_samples) {
          invoke('js2rs', {
            message: JSON.stringify({
              command: "transport",
              action: "loop",
              start: _this.loop_region.selected_samples.start,
              end: _this.loop_region.selected_samples.end
            })
          });
        } else {
/*
        let { numerator, denominator } = _this.session.time_signature;

        let tick_samples = 60 / _this.session.bpm * 4 / denominator * _this.session.sample_rate;
        let bar_samples = tick_samples * numerator;

          invoke('js2rs', {
            message: JSON.stringify({
              command: "transport",
              action: "loop",
              start: 0,
              end: bar_samples
            })
          });*/
        }
      }
    });

    this.loop_region = new SampleRegion(this.session, "loop");


    setInterval(() => {
      _this.update();
      up_cb();
    }, 33);

  }

  set_ms(npos_ms) {
    this.position.ms = npos_ms;
    this.position.samples = Math.floor(npos_ms / 1000 * this.session.sample_rate);
  }

  set_samples(npos_samples) {
    this.position.samples = npos_samples;
    this.position.ms = Math.floor(npos_samples / this.session.sample_rate * 1000);
  }

  time_string() {
    let ms = this.position.ms;
    let secs = Math.floor(ms / 1000);
    let mins = Math.floor(secs / 60);

    let hrs = Math.floor(mins / 60).toString();
    mins = (mins % 60).toString();
    secs = (secs % 60).toString();
    ms = (ms % 1000).toString();

    if (hrs.length == 1) hrs = "0"+hrs;
    if (mins.length == 1) mins = "0"+mins;
    if (secs.length == 1) secs = "0"+secs;
    if (ms.length < 4) {
      let nn = 3 - ms.length;
      let nstr = "";
      for (let n = 0; n < nn; n++) {
        nstr += "0";
      }
      ms = nstr+ms;
    }

    return `${hrs}:${mins}:${secs}:${ms}`;
  }

  update() {
    if (this.is_playing) {
      const current_time = Date.now();
      const elapsed_time = current_time - this.last_update_time;

      this.set_ms(this.position.ms + elapsed_time);

      if (
        this.is_looping &&
        this.loop_region.selected_samples &&
        (this.loop_region.selected_samples.end <= this.position.samples)
      ) {
        let loop_distance = this.loop_region.selected_samples.end - this.loop_region.selected_samples.start;
        this.set_samples(this.position.samples - loop_distance);
      }

      time_screen.innerText = this.time_string();
      transport_div.style.left = this.session.samples_to_pixels(this.position.samples) + "px";
      this.last_update_time = current_time;
    } else {
      this.last_update_time = null;
    }
  }

  loop(start, end) {
    this.is_looping = true;


    if (this.selected_region.selected_samples) {
      this.loop_region.selected_pixels = this.selected_region.selected_pixels;
      this.loop_region.selected_samples = this.selected_region.selected_samples;
    }
    this.loop_region.update();

    if (track_bodies.contains(this.selected_region.element)) {
      this.selected_region.deselect();
      track_bodies.removeChild(this.selected_region.element);
    }

    if (!track_bodies.contains(this.loop_region.element)) {
      track_bodies.appendChild(this.loop_region.element);
    }

    if (this.loop_region.element.classList.contains("inactive")) {
      this.loop_region.element.classList.remove("inactive");
    }
  }

  end_loop() {
    this.is_looping = false;
    this.loop_region.element.classList.add("inactive");
  }


  zoom() {
    this.selected_region.update();
    this.loop_region.update();
  }

  cmd(data) {
    if (data.play) {
      this.last_update_time = Date.now();
      play_pause_toggle.innerText = "pause";
      play_pause_toggle.classList.add("active");
      this.is_playing = true;
    } else if (data.pause) {
      play_pause_toggle.innerText = "play";
      play_pause_toggle.classList.remove("active");
      this.set_samples(data.pause.position);
      time_screen.innerText = this.time_string();
      transport_div.style.left = this.position.ms / 1000 * this.session.px_per_sec + "px";
      this.is_playing = false;
    } else if (data.stop) {
      play_pause_toggle.innerText = "play";
      play_pause_toggle.classList.remove("active");
      this.set_ms(0);
      time_screen.innerText = this.time_string();
      transport_div.style.left = this.position.ms / 1000 * this.session.px_per_sec + "px";
      this.is_playing = false;
    } else if (data.loop) {
      this.loop(data.loop.start, data.loop.end);
    } else if (data.end_loop) {
      this.end_loop();
    }

  }
}
