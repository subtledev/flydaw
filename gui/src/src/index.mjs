

import { listen } from '@tauri-apps/api/event'
import { invoke } from '@tauri-apps/api/tauri'

import Editor from './editor/index.mjs'


(async () => {
  try {
    const editor = new Editor();

    function sendOutput(output) {
      invoke('js2rs', { message: output }) 
    }

    const command_log = document.getElementById("command-log");

    await listen('rs2js', (event) => {
      const data = JSON.parse(event.payload);
      editor.cmd(data);

      console.log("js: rs2js: " + JSON.stringify(data, null, 2))
    })

    const add_audio_input = document.getElementById("add-audio-track");
    add_audio_input.addEventListener("click", (e) => {
      sendOutput(JSON.stringify({
        command: "mixer",
        action: "add_track",
        type: "audio"
      }));
    });

    const add_midi_input = document.getElementById("add-midi-track");
    add_midi_input.addEventListener("click", (e) => {
      sendOutput(JSON.stringify({
        command: "mixer",
        action: "add_track",
        type: "midi"
      }));
    });
  } catch (e) {
    console.error(e);
  }
})();

