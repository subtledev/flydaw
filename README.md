# FlyDaw - digital audio workstation

Project aim is to write a headless DAW that would be lightweight and could be used with different GUI implementations.
Currently spawns a separate tauri app process for the GUI and communicates with it sending JSON commands over `stdio`.

## Requirements:

- JACK Audio Connection Kit (or JACK)

## Prepare development environment:
- `./dev-prep.sh`

## Compile and run:

- `cd daw && cargo run`

*Note: will crash if JACK is not started.*

