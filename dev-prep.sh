#!/bin/bash

cwd=$(pwd)
run_gui_dev_cmd="#!/bin/bash\n\ncd \"\$(dirname \"\$0\")\" && npm run tauri build -- --debug;\n$cwd/gui/src-tauri/target/debug/tauri-app"

echo "Writing GUI runner script to: $cwd/gui/run-gui-dev.sh"
echo -e $run_gui_dev_cmd > "$cwd/gui/run-gui-dev.sh"
chmod +x "$cwd/gui/run-gui-dev.sh"

echo "Installing GUI dependencies"
cd "$cwd/gui" && npm install

echo "All done! Run \`cd daw\` and \`cargo run\` to test."

