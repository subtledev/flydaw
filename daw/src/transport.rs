use std::sync::{Arc, Mutex};

use serde_json::json;


use crate::GUIProcess;
use crate::session::config::SessionConfig;


pub struct Transport {
    pub is_ready: bool,
    pub is_playing: bool,
    pub is_recording: bool,
    position: usize,
    pub session: Arc<Mutex<SessionConfig>>,
    gui: Option<Arc<Mutex<GUIProcess>>>,
    pub loop_region: Option<(usize, usize)>,
}

impl Transport {

    pub fn new(session: Arc<Mutex<SessionConfig>>) -> Transport {
        Transport {
            is_ready: true,
            is_playing: false,
            is_recording: false,
            gui: None,
            session,
            position: 0,
            loop_region: None
        }
    }

    pub fn play(&mut self) {
        self.is_playing = true;
        if let Some(gui_mutex) = self.gui.as_ref() {
            let mut gui = gui_mutex.lock().unwrap();
            gui.send(json!({
                "command": "transport",
                "play": {
                    "position": self.position,
                }
            }));
        }
    }

    pub fn loop_samples(&mut self, start: usize, end: usize) {
        self.loop_region = Some(
            (start, end)
        );
        self.session.lock().unwrap().loop_region = Some(
            (start, end)
        );
        if let Some(gui_mutex) = self.gui.as_ref() {
            let mut gui = gui_mutex.lock().unwrap();
            gui.send(json!({
                "command": "transport",
                "loop": {
                    "start": start,
                    "end": end
                }
            }));
        }
    }

    pub fn end_loop(&mut self) {
        self.loop_region = None;
        self.session.lock().unwrap().loop_region = None;

        if let Some(gui_mutex) = self.gui.as_ref() {
            let mut gui = gui_mutex.lock().unwrap();
            gui.send(json!({
                "command": "transport",
                "end_loop": true
            }));
        }
    }

    pub fn pause(&mut self) {
        self.is_playing = false;
        if let Some(gui_mutex) = self.gui.as_ref() {
            let mut gui = gui_mutex.lock().unwrap();
            gui.send(json!({
                "command": "transport",
                "pause": {
                    "position": self.position,
                }
            }));
        }
    }

    pub fn stop(&mut self) {
        self.position = 0;
        self.is_playing = false;
        if let Some(gui_mutex) = self.gui.as_ref() {
            let mut gui = gui_mutex.lock().unwrap();
            gui.send(json!({
                "command": "transport",
                "stop": true
            }));
        }
    }

    pub fn next_frame(&mut self) -> usize {

        if let Some( (loop_start, loop_end) ) = self.loop_region.as_ref() {
            if (self.position < *loop_start || self.position > *loop_end) {
                self.position = loop_start.clone();
            } else {
                let mut session = self.session.lock().unwrap();
                self.position = self.position + session.frame_size;
                drop(session);
                
                if (*loop_end < self.position) {
                    self.position = loop_start + (self.position - loop_end);
                }
            }
        } else {
            let mut session = self.session.lock().unwrap();
            self.position = self.position + session.frame_size;
            drop(session);
        }


//        println!("next frame: {}", self.position);

        self.position
    }

    pub fn set_gui(&mut self, gui: Arc<Mutex<GUIProcess>>) {
        self.gui = Some(gui);
    }

    pub fn get_position(&mut self) -> usize {
        self.position
    }
}
