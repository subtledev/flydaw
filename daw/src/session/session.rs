use serde_yaml::Value;
use std::fs::File;
use std::io::{Read, Write};
use std::path::Path;

use std::sync::{Arc, Mutex};

use crate::Mixer;


use crate::GUIProcess;

use crate::session::config::SessionConfig;

pub struct Session {
    pub session_config: Arc<Mutex<SessionConfig>>,
    mixer: Arc<Mutex<Mixer>>,
    gui: Arc<Mutex<GUIProcess>>,
    current_session_path: Option<String>
}

impl Session {

    pub fn new(session_config: Arc<Mutex<SessionConfig>>, mixer: Arc<Mutex<Mixer>>, gui: Arc<Mutex<GUIProcess>>) -> Session {
        Session {
            session_config,
            mixer,
            gui,
            current_session_path: None
        }
    }

    pub fn load(&mut self, session_path: &str) {
        println!("LOAD SESSION");
        let session_cfg = self.session_config.lock().unwrap();
        let full_base_path = session_cfg.full_base_path.clone();
        let base_path = Path::new(full_base_path.as_str());
        drop(session_cfg);
        self.current_session_path = Some(String::from(session_path));
        let csess_path = self.current_session_path.clone().unwrap();
        let session_fdaw_path = Path::new(csess_path.as_str());
        let mut session_file = File::open(&session_fdaw_path).expect("Failed to open session file!");

        let mut content = String::new();
        session_file.read_to_string(&mut content).unwrap();

        let session_config: Value = serde_yaml::from_str(&content).expect("Failed to parse session YAML!");

        if let Some(session_name) = session_config.get("name") {
            if let Some(string_value) = serde_yaml::to_string(session_name).ok() {
                println!("Session name: {}", string_value.trim());
            }
        }

        let mut mixer = self.mixer.lock().unwrap();
        mixer.clear();

        if let Some(tracks) = session_config.get("tracks").and_then(Value::as_sequence) {
            for (_index, element) in tracks.iter().enumerate() {

                if let Some(track_type) = element.get("type") {
                    if let Some(type_value) = serde_yaml::to_string(track_type).ok() {
                        let create = type_value.trim();
                        if let Some(track_name) = element.get("name") {
                            if let Some(name_value) = serde_yaml::to_string(track_name).ok() {
                                let tname = name_value.trim();
                                println!("Track: {}", tname);
                                println!("Create: {}", create);
                                if create == "audio" {
                                    mixer.add_audio_track(Some(
                                        name_value.trim().to_string()
                                    ));
                                } else if create == "midi" {
                                    mixer.add_midi_track(Some(
                                        name_value.trim().to_string()
                                    ));
                                }
                                println!("!track added");



                                if let Some(control_states) = element.get("control_states") {
                                    if let Some(mute_state) = control_states.get("mute") {
                                        if let Some(bool_value) = serde_yaml::from_value::<bool>(mute_state.clone()).ok() {
                                            mixer.set_track_state(name_value.trim().to_string(), "mute".to_string(), bool_value);
                                        }
                                    }
                                    if let Some(solo_state) = control_states.get("solo") {
                                        if let Some(bool_value) = serde_yaml::from_value::<bool>(solo_state.clone()).ok() {
                                            mixer.set_track_state(name_value.trim().to_string(), "solo".to_string(), bool_value);
                                        }
                                    }
                                    if let Some(record_state) = control_states.get("record") {
                                        if let Some(bool_value) = serde_yaml::from_value::<bool>(record_state.clone()).ok() {
                                            mixer.set_track_state(name_value.trim().to_string(), "record".to_string(), bool_value);
                                        }
                                    }
                                }
                                

                                if let Some(sample_cuts) = element.get("sample_cuts").and_then(Value::as_sequence) {
                                    for (_i, sample_cut) in sample_cuts.iter().enumerate() {
//                                        if let Some(cut_id) = sample_cut.get("id") {
//                                            if let Some(id_value) = serde_yaml::from_value::<usize>(cut_id.clone()).ok() {
                                                if let Some(cut_rel_path) = sample_cut.get("rel_path") {
                                                    if let Some(path_value) = serde_yaml::to_string(cut_rel_path).ok() {
                                                        if let Some(cut_position) = sample_cut.get("position") {
                                                            if let Some(pos_value) = serde_yaml::from_value::<f32>(cut_position.clone()).ok() {
                                                                let mut files_vec: Vec<String> = Vec::new();
                                                                let session_cfg = self.session_config.lock().unwrap();
                                                                let samples_dir_path = session_cfg.samples_dir_path.clone();
                                                                let samples_path = Path::new(samples_dir_path.as_str());
                                                                drop(session_cfg);
                                                                let cut_path = samples_path.join(tname).join(path_value.trim());
                                                                files_vec.push(cut_path.to_string_lossy().to_string());
                                                                mixer.import_files(name_value.trim().to_string(), files_vec, pos_value);
                                                            }
                                                        }
                                                    }
                                                }
//                                            }
//                                        }
                                    }
                                } else if let Some(seqs) = element.get("sequences").and_then(Value::as_sequence) {
                                    for (_i, seq) in seqs.iter().enumerate() {
//                                        if let Some(cut_id) = seq.get("id") {
//                                            if let Some(id_value) = serde_yaml::from_value::<usize>(cut_id.clone()).ok() {
                                                if let Some(cut_rel_path) = seq.get("rel_path") {
                                                    if let Some(path_value) = serde_yaml::to_string(cut_rel_path).ok() {
                                                        if let Some(cut_position) = seq.get("position") {
                                                            if let Some(pos_value) = serde_yaml::from_value::<f32>(cut_position.clone()).ok() {
                                                                let mut files_vec: Vec<String> = Vec::new();
                                                                let session_cfg = self.session_config.lock().unwrap();
                                                                let samples_dir_path = session_cfg.samples_dir_path.clone();
                                                                let samples_path = Path::new(samples_dir_path.as_str());
                                                                drop(session_cfg);
                                                                let cut_path = samples_path.join(tname).join(path_value.trim());
                                                                files_vec.push(cut_path.to_string_lossy().to_string());
                                                                mixer.import_midi_files(name_value.trim().to_string(), files_vec, pos_value);
                                                            }
                                                        }
                                                    }
                                                }
//                                            }
//                                        }
                                    }
                                }


                            }
                        }
                    }
                }



            }
        } else {
            println!("Array property not found or is not an array");
        }

        println!("LOADED TRACKS");

        if let Some(cfg) = session_config.get("config") {
            if
                let Some(bpm) = cfg.get("bpm")
                    .and_then(Value::as_f64)
                    .map(|v| v as f32)
            {
                self.session_config.lock().unwrap().bpm = bpm;
                    let mut gui = self.gui.lock().unwrap();
                    gui.send(serde_json::json!({
                        "command": "session",
                        "bpm": bpm
                    }));
            }

            if
                let Some(time_signature) = cfg.get("time_signature")
            {
                let numerator = time_signature.get("numerator")
                    .and_then(Value::as_u64)
                    .map(|v| v as usize)
                    .unwrap_or_default();
                let denominator = time_signature.get("denominator")
                    .and_then(Value::as_u64)
                    .map(|v| v as usize)
                    .unwrap_or_default();
                self.session_config.lock().unwrap().time_signature = (numerator, denominator);
                    let mut gui = self.gui.lock().unwrap();
                    gui.send(serde_json::json!({
                        "command": "session",
                        "time_signature": {
                            "numerator":  numerator,
                            "denominator":  denominator
                        }
                    }));
            }
        }
    }

    pub fn save(&mut self, session_path: Option<&str>) {
        if let Some(session_fdaw_path) = session_path {
            self.current_session_path = Some(String::from(session_fdaw_path));
        }


            println!("LOCK CFG");
        let session_cfg = self.session_config.lock().unwrap();
        let session_name = "untitled";
        let bpm = session_cfg.bpm;
        let (ts_numerator, ts_denominator) = session_cfg.time_signature;
        drop(session_cfg);

            println!("CFG RDY");

        if let Some(session_fdaw_path) = &self.current_session_path {
            let sess_path = Path::new(session_fdaw_path.as_str());

            let mut session_file = File::create(&sess_path)
                .expect("Failed to open session file!");

            println!("PREP JSON");
            let mut session_json = serde_json::json!({
                "name": session_name,
                "config": {
                    "bpm": bpm,
                    "time_signature": {
                        "numerator": ts_numerator,
                        "denominator": ts_denominator
                    }
                },
                "tracks": []
            });

            let mut mixer = 
            for atrack in self.mixer.lock().unwrap().audio_tracks.iter() {
                let mut ctl_states = atrack.track.control_states.lock().unwrap();
                let mut ntrack = serde_json::json!({
                    "name": atrack.track.name,
                    "type": "audio",
                    "control_states": {
                        "solo": ctl_states.get("solo"),
                        "mute": ctl_states.get("mute"),
                        "record": ctl_states.get("record")
                    },
                    "sample_cuts": []
                });

                for scut in &atrack.sample_cuts {
                    let nscut = serde_json::json!({
                        "id": scut.id,
                        "relpath": scut.full_path,
                        "position": scut.position
                    });
                    ntrack["sample_cuts"].as_array_mut().unwrap().push(nscut);
                }

                session_json["tracks"].as_array_mut().unwrap().push(ntrack);
            };


            let session_yaml = serde_yaml::to_value(&session_json).unwrap();

            session_file.write_all(serde_yaml::to_string(&session_yaml).unwrap().as_bytes());
        }
    }
}
