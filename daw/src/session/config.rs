

use std::sync::{Arc, Mutex};
use std::path::Path;

use crate::Session;

pub struct SessionConfig {
    pub full_base_path: String,
    pub samples_dir_path: String,
    pub bpm: f32,
    pub ticks_per_beat: usize,
    pub sample_rate: usize,
    pub frame_size: usize,
    pub time_signature: (usize, usize),
    pub metronome_on: bool,
    pub loop_region: Option<(usize, usize)>,
    pub session: Option<Arc<Mutex<Session>>>
}

impl SessionConfig {

    pub fn new(sample_rate: usize, frame_size: usize) -> SessionConfig {

        // Base directory path
        let base_path = Path::new("/home/qualphey/FlyDaw/test-session");


        // Create the full path to the config file
        let samples_dir_path = base_path.join("tracks");
        println!("BASE: {}", base_path.to_string_lossy().to_string());
        println!("SAMPLES: {}", samples_dir_path.to_string_lossy().to_string());


        SessionConfig {
            full_base_path: base_path.to_string_lossy().to_string(),
            samples_dir_path: samples_dir_path.to_string_lossy().to_string(),
            bpm: 120.0,
            ticks_per_beat: 480,
            sample_rate,
            frame_size,
            time_signature: (4, 4),
            metronome_on: false,
            loop_region: None,
            session: None
        }
    }

    pub fn ticks_to_frames(
        &mut self,
        ticks: u32
    ) -> u32 {
        let seconds_per_tick = 60.0 / (self.bpm * self.ticks_per_beat as f32);
        println!("TTF RESULT: {}", ((self.sample_rate as f32 * seconds_per_tick * ticks as f32).round()) as u32);
        ((self.sample_rate as f32 * seconds_per_tick * ticks as f32).round()) as u32
    }

    pub fn set_session(&mut self, session: Arc<Mutex<Session>>) {
        self.session = Some(session);
    }
}
