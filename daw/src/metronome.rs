use std::sync::{Arc, Mutex};

use crate::jack::queue::audio::playback::AudioPlaybackQueue;

use crate::session::config::SessionConfig;
use crate::JackClient;

use crate::globals;

const FREQUENCY: f32 = 1.0;

pub struct Metronome {
    session: Arc<Mutex<SessionConfig>>,
    jack_client: Arc<Mutex<JackClient>>,
    playback_queue: Arc<Mutex<AudioPlaybackQueue>>,
    tick_sample: Vec<f32>,
    last_tick: usize,
    pub seek_position: usize
}

impl Metronome {

    pub fn new(
        jack_client: Arc<Mutex<JackClient>>,
        session: Arc<Mutex<SessionConfig>>
    ) -> Metronome {

        let mut session_mut = session.lock().unwrap();
        let sample_rate = session_mut.sample_rate as f32;
        let frame_size = session_mut.frame_size;
        drop(session_mut);

        let samples_per_period = (sample_rate / FREQUENCY) as usize;

        let mut playback_queue = AudioPlaybackQueue::new();
        playback_queue.set_frame_size(frame_size);
        let playback_queue_arc = Arc::new(Mutex::new(playback_queue));

        let jc_mutex = jack_client.as_ref();
        let mut jc = jc_mutex.lock().unwrap();
        jc.add_metronome(Arc::clone(&playback_queue_arc));
        drop(jc);

        let mut tick_sample: Vec<f32> = (0..samples_per_period)
            .map(|x| (2.0 * std::f32::consts::PI * FREQUENCY * x as f32 / sample_rate as f32).sin())
            .collect();
        let sample_silence = vec![0.0; tick_sample.len() % frame_size];
        tick_sample.extend(sample_silence);

        Metronome {
            session,
            jack_client,
            playback_queue: playback_queue_arc,
            tick_sample,
            last_tick: 0,
            seek_position: 0
        }
    }

    pub fn update(&mut self, position: usize) {
        let mut session_mut = self.session.lock().unwrap();
        let sample_rate = session_mut.sample_rate as f64;
        let frame_size = session_mut.frame_size;
        let bpm = session_mut.bpm as f64;
        let (numerator, denominator) = session_mut.time_signature;
        drop(session_mut);

//        let bar_duration = (60.0 / bpm) * numerator as f64;
//        let tick_time = (bar_duration / numerator as f64 * sample_rate) as usize; 

        let tick_sample_length = self.tick_sample.len();

        let tick_time = 60.0 / bpm * 4.0 / denominator as f64 * sample_rate as f64;
        let bar_duration = (tick_time * numerator as f64) as usize;

        if self.seek_position < position {
            self.seek_position = position;
        }


        let mut start_position =  ((self.seek_position as f64 / tick_time as f64).floor() * tick_time as f64) as usize;

        let mut frame_adjust = start_position % frame_size;
        if frame_adjust == frame_size {
            frame_adjust = 0;
        }
//        println!("frame_adjust: {}", frame_adjust);


        start_position -= frame_adjust;
        let mut end_position = start_position + (1.0/FREQUENCY*sample_rate as f32) as usize;
        end_position -= frame_adjust;

        if self.seek_position <= position + (globals::QUEUE_LENGTH * frame_size) {
/*            println!(
                "start: {}; end: {}; pos: {}, seek: {}, tick_time: {}, tick_sample_length: {}",
                start_position,
                end_position,
                position,
                self.seek_position,
                tick_time,
                self.tick_sample.len()
            );*/

            if start_position <= self.seek_position && self.seek_position < end_position {
                let real_seek_pos = self.seek_position - start_position;
                let real_seek_end = real_seek_pos + frame_size;

//                println!("{} - {}", real_seek_pos, real_seek_end);
                let next_frame: Vec<f32> = self.tick_sample.clone().drain(real_seek_pos..real_seek_end).collect();

                let as_array: [Vec<f32>; 2] = [next_frame.clone(), next_frame.clone()];
                
//                    println!("PROGRESS: {:?}", as_array);
                self.playback_queue.lock().unwrap().progress(as_array);
            } else {
//                println!("ADD EMPTY FRAME");
                let as_array: [Vec<f32>; 2] = [vec![0.0; frame_size].clone(), vec![0.0; frame_size].clone()];
                self.playback_queue.lock().unwrap().progress(as_array);
            }
            self.seek_position = self.seek_position + frame_size;
        }

    }
}
