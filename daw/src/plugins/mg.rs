

use crate::plugins::lv2::host::LV2Host;

pub struct PluginsManager {
    lv2_host: Option<LV2Host>
}


impl PluginsManager {

    pub fn new() -> PluginsManager {
        PluginsManager {
            lv2_host: Some(LV2Host::new())
        }
    }
}
