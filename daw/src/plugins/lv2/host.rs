


pub struct LV2Host {

}

impl LV2Host {
    pub fn new() -> LV2Host {
//        println!("LV2Host >>>>>>>>>>>>>>>>>>>>>>>>>>");

        let world = livi::World::new();
        const SAMPLE_RATE: f64 = 48000.0;
        let features = world.build_features(livi::FeaturesBuilder {
            min_block_length: 1,
            max_block_length: 4096,
        });
        let plugin = world
            .plugin_by_uri("http://lsp-plug.in/plugins/lv2/multisampler_x24")
            .expect("Plugin not found.");
        let mut instance = unsafe {
            plugin
                .instantiate(features.clone(), SAMPLE_RATE)
                .expect("Could not instantiate plugin.")
        };

        let input = {
            let mut s = livi::event::LV2AtomSequence::new(&features, 1024);
            let play_note_data = [0x90, 0x40, 0x7f];
            s.push_midi_event::<3>(1, features.midi_urid(), &play_note_data)
                .unwrap();
            s
        };

        // This is where the audio data will be stored.
        let mut outputs = [
            vec![0.0; features.max_block_length()], // For mda EPiano, this is the left channel.
            vec![0.0; features.max_block_length()], // For mda EPiano, this is the right channel.
        ];

//        println!("{:?}", plugin);

        let audio_input_ports = plugin.ports_with_type(livi::PortType::AudioInput);
        let audio_output_ports = plugin.ports_with_type(livi::PortType::AudioOutput);

        let event_input_ports = plugin.ports_with_type(livi::PortType::AtomSequenceInput);
        let event_output_ports = plugin.ports_with_type(livi::PortType::AtomSequenceOutput);

//        let control_input_ports = plugin.ports_with_type(livi::PortType::ControlInput);
//        let control_output_ports = plugin.ports_with_type(livi::PortType::ControlOutput);
/*
        for port in audio_input_ports {
            println!("{:?}", port);
        }
        for port in audio_output_ports {
            println!("{:?}", port);
        }
        for port in event_input_ports {
            println!("{:?}", port);
        }
        for port in event_output_ports {
            println!("{:?}", port);
        }*/
/*
        println!(
            "{:?} {:?} {:?} {:?}",
            audio_input_ports,
            audio_output_ports,
            event_input_ports,
            event_output_ports
        );*/


//        println!("LV2Host <<<<<<<<<<<<<<<<<<<<");

        // Set up the port configuration and run the plugin!
        // The results will be stored in `outputs`.
/*        let ports = livi::EmptyPortConnections::new()
            .with_atom_sequence_inputs(std::iter::once(&input))
            .with_audio_outputs(outputs.iter_mut().map(|output| output.as_mut_slice()));
        unsafe { instance.run(features.max_block_length(), ports).unwrap() };*/

        LV2Host {

        }
    }

}
