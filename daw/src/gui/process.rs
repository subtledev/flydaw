
use std::env;
use std::process::Stdio;
use std::sync::{Arc, Mutex};
use std::thread;
use std::time::Duration;

use tokio::io::{AsyncBufReadExt, AsyncWriteExt, BufReader, BufWriter};
use tokio::process::{Command};

use crate::gui::cmd::handler::CommandHandler;
use crate::gui::cmd::cut::CutCommandHandler;
use crate::gui::cmd::mixer::MixerCommandHandler;
use crate::gui::cmd::plugin::PluginCommandHandler;
use crate::gui::cmd::process::ProcessCommandHandler;
use crate::gui::cmd::session::SessionCommandHandler;
use crate::gui::cmd::transport::TransportCommandHandler;

use crate::Mixer;
use crate::Transport;

pub struct GUIProcess {
    child_process: tokio::process::Child,
    output_queue: Arc<Mutex<Vec<String>>>,
}

impl GUIProcess {

    pub fn new() -> GUIProcess {
        let current_exe = env::current_exe().unwrap_or_else(|err| {
            eprintln!("Failed to determine the current executable path: {}", err);
            std::process::exit(1);
        });

        let exe_directory = current_exe.parent().unwrap();
        let command_path = exe_directory.join("../../../gui/run-gui-dev.sh");

        // Create a new Command
        let mut child_process = Command::new(&command_path)
            .stdin(Stdio::piped())
            .stdout(Stdio::piped())
            .spawn()
            .expect("Failed to spawn child process");

        GUIProcess {
            child_process,
            output_queue: Arc::new(Mutex::new(Vec::new())),
        }
    }

    pub async fn initialize_communications(
        &mut self,
        mixer: Arc<Mutex<Mixer>>,
        transport: Arc<Mutex<Transport>>,
        gui: Arc<Mutex<GUIProcess>>
    ) {
        let output_queue_arc = self.output_queue.clone();

        let session_arc = Arc::clone(&transport.lock().unwrap().session);

        let gui_ready: Arc<Mutex<bool>> = Arc::new(Mutex::new(false));
        let gui_ready_clone = Arc::clone(&gui_ready);

        if let (Some(child_stdout), Some(child_stdin)) = (self.child_process.stdout.take(), self.child_process.stdin.take()) {
            let _child_stdout_reader = tokio::spawn(async move {
                let mut process_ch = ProcessCommandHandler::construct((gui_ready_clone));
                let mut mixer_ch = MixerCommandHandler::construct((Arc::clone(&mixer)));
                let mut plugin_ch = PluginCommandHandler::construct(());
                let mut cut_ch = CutCommandHandler::construct((Arc::clone(&mixer)));
                let mut session_ch = SessionCommandHandler::construct((Arc::clone(&session_arc), Arc::clone(&gui)));
                let mut transport_ch = TransportCommandHandler::construct((Arc::clone(&transport)));

                let mut child_reader = BufReader::new(child_stdout).lines();
                while let Some(line) = child_reader.next_line().await.unwrap() {
                    println!("RECV {}", line);
                    let result: Result<serde_json::Value, serde_json::Error> = serde_json::from_str(line.as_str());
                    match result {
                        Ok(json_value) => {
                            let cmd = json_value.get("command").and_then(|value| value.as_str())
                                .map(String::from).unwrap_or_else(|| String::new());
                            match cmd.as_str() {
                                "process" => process_ch.handle(json_value),
                                "mixer" => mixer_ch.handle(json_value),
                                "plugin" => plugin_ch.handle(json_value),
                                "cut" => cut_ch.handle(json_value),
                                "session" => session_ch.handle(json_value),
                                "transport" => transport_ch.handle(json_value),
                                &_ => println!("Invalid incomming GUI command {}", cmd)
                            }
                        }
                        Err(e) => {
                            println!("Error parsing JSON: {}", e);
                        }
                    }
                }
            });
            let _child_stdin_writer = tokio::spawn(async move {
                let output_queue = Arc::clone(&output_queue_arc);

                let mut child_writer = BufWriter::new(child_stdin);

                loop {
                    if output_queue.lock().unwrap().len() > 0 {
                        let values_to_process: Vec<_> = output_queue.lock().unwrap().iter().cloned().collect();
                        output_queue.lock().unwrap().clear(); // Clear the queue
                        
                        for first_value in values_to_process {
                            println!("Processing value: {}", first_value);
                            child_writer.write_all(first_value.as_bytes()).await.expect("Failed to write to child stdin");
                            child_writer.flush().await.expect("Failed to flush child stdin");
                        }
                    }
                }
            });
        }

        let wait_ready = tokio::spawn(async move {
            while !(*gui_ready.lock().unwrap()) {
                thread::sleep(Duration::from_millis(333));
            }
        });
        wait_ready.await.expect("Failed to start GUI.");
    }

    pub fn send<T>(&mut self, data: T) where T: serde::Serialize {
        let json_value = serde_json::to_value(data).expect("Failed to serialize to JSON");
        let mut output_queue = self.output_queue.lock().unwrap();
        output_queue.push(format!("{}\n", json_value.to_string()));
    }
}
