
use std::sync::{Arc, Mutex};

use crate::Mixer;

use crate::gui::cmd::handler::CommandHandler;

pub struct CutCommandHandler {
    mixer: Arc<Mutex<Mixer>>
}

impl CommandHandler for CutCommandHandler {
    type ConstructorArgs = (Arc<Mutex<Mixer>>);

    fn construct(args: Self::ConstructorArgs) -> CutCommandHandler {
        let (mixer) = args;
        CutCommandHandler {
            mixer
        }
    }

    fn handle(&mut self, json_value: serde_json::Value) {
        let action = json_value.get("action").and_then(|value| value.as_str())
            .map(String::from).unwrap_or_else(|| String::new());
        match action.as_str() {
            "remove" => {
                let track_name = json_value
                    .get("track")
                    .and_then(|value| value.as_str())
                    .map(String::from)
                    .unwrap_or_else(|| String::new());
                let cut_id = json_value
                    .get("id")
                    .and_then(serde_json::Value::as_u64)
                    .map(|v| v as usize)
                    .unwrap_or_default();

                self.mixer.lock().unwrap().remove_sample_cut(track_name, cut_id);
            }
            "set_position" => {
                let track_name = json_value
                    .get("track")
                    .and_then(|value| value.as_str())
                    .map(String::from)
                    .unwrap_or_else(|| String::new());
                let cut_id = json_value
                    .get("cut_id")
                    .and_then(serde_json::Value::as_u64)
                    .map(|v| v as usize)
                    .unwrap_or_default();
                let position = json_value
                    .get("position")
                    .and_then(serde_json::Value::as_u64)
                    .map(|v| v as usize)
                    .unwrap_or_default();

                self.mixer.lock().unwrap().move_sample_cut(track_name, cut_id, position);
            }
            "split" => {
                // TODO
            }
            "set_start" => {
                // TODO
            }
            "set_end" => {
                // TODO
            }
            "set_fadein" => {
                // TODO
            }
            "set_fedeout" => {
                // TODO
            }
            "set_layer" => {
                // TODO
            }
            &_ => {
                println!("Invalid cut action `{}`", action);
            }
        }
    }
}
