

pub trait CommandHandler {
    type ConstructorArgs;
    fn construct(args: Self::ConstructorArgs) -> Self;
    fn handle(&mut self, json_value: serde_json::Value);
}
