
use std::sync::{Arc, Mutex};

pub struct ProcessCommandHandler {
    gui_ready: Arc<Mutex<bool>>
}


use crate::gui::cmd::handler::CommandHandler;

impl CommandHandler for ProcessCommandHandler {
    type ConstructorArgs = (Arc<Mutex<bool>>);


    fn construct(args: (Arc<Mutex<bool>>)) -> ProcessCommandHandler {
        let (gui_ready) = args;
        ProcessCommandHandler {
            gui_ready
        }
    }

    fn handle(&mut self, json_value: serde_json::Value) {
        let action = json_value.get("action").and_then(|value| value.as_str())
            .map(String::from).unwrap_or_else(|| String::new());

        match action.as_str() {
            "ready" => {
                *(self.gui_ready.lock().unwrap()) = true;
            }
            "exit" => {

            }
            &_ => {
                println!("Invalid process action `{}`", action);
            }
        }
    }
}
