
use std::sync::{Arc, Mutex};

use crate::Mixer;


use crate::gui::cmd::handler::CommandHandler;

pub struct MixerCommandHandler {
    mixer: Arc<Mutex<Mixer>>
}

impl CommandHandler for MixerCommandHandler {
    type ConstructorArgs = (Arc<Mutex<Mixer>>);

    fn construct(args: (Arc<Mutex<Mixer>>)) -> MixerCommandHandler {
        let (mixer) = args;
        MixerCommandHandler {
            mixer
        }
    }

    fn handle(&mut self, json_value: serde_json::Value) {
        let action = json_value.get("action").and_then(|value| value.as_str())
            .map(String::from).unwrap_or_else(|| String::new());

        match action.as_str() {
            "add_track" => {
                let track_type = json_value
                    .get("type")
                    .and_then(|value| value.as_str())
                    .map(String::from)
                    .unwrap_or_else(|| String::new());
                if track_type == "audio" {
                    self.mixer.lock().unwrap().add_audio_track(None);
                } else if track_type == "midi" {
                    self.mixer.lock().unwrap().add_midi_track(None);
                }
            }
            "remove_track" => {
                let track_name = json_value
                    .get("name")
                    .and_then(|value| value.as_str())
                    .map(String::from)
                    .unwrap_or_else(|| String::new());
                let track_type = json_value
                    .get("type")
                    .and_then(|value| value.as_str())
                    .map(String::from)
                    .unwrap_or_else(|| String::new());
                if track_type == "audio" {
                    self.mixer.lock().unwrap().remove_audio_track(track_name);
                } else if track_type == "midi" {
                    self.mixer.lock().unwrap().remove_midi_track(track_name);
                }
            }
            "set_track_state" => {
                let track_name = json_value
                    .get("track_name")
                    .and_then(|value| value.as_str())
                    .map(String::from)
                    .unwrap_or_else(|| String::new());
                let which = json_value
                    .get("which")
                    .and_then(|value| value.as_str())
                    .map(String::from)
                    .unwrap_or_else(|| String::new());
                let state = json_value
                    .get("state")
                    .and_then(|value| value.as_bool())
                    .unwrap_or(false);
                self.mixer.lock().unwrap().set_track_state(track_name, which, state);
            }
            "set_gain_level" => {
                // TODO
            }
            "import_files" => {
                let track = json_value
                    .get("track")
                    .and_then(|value| value.as_str())
                    .map(String::from)
                    .unwrap_or_else(|| String::new());

                let files = json_value
                    .get("files")
                    .and_then(|value| value.as_array())
                    .map(|array| array.iter().filter_map(|v| v.as_str().map(String::from)).collect())
                    .unwrap_or_else(|| Vec::new());

                self.mixer.lock().unwrap().import_files(track, files, 0.0);
            }
            &_ => {
                println!("Invalid mixer action `{}`", action);
            }
        }
    }
}
