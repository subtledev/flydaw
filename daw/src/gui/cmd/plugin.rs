
pub struct PluginCommandHandler {

}


use crate::gui::cmd::handler::CommandHandler;

impl CommandHandler for PluginCommandHandler {
    type ConstructorArgs = ();

    fn construct(args: ()) -> PluginCommandHandler {
        PluginCommandHandler {

        }
    }

    fn handle(&mut self, json_value: serde_json::Value) {
        let action = json_value.get("action").and_then(|value| value.as_str())
            .map(String::from).unwrap_or_else(|| String::new());

        match action.as_str() {
            "list" => {
                // TODO
            }
            "add" => {
                // TODO
            }
            "remove" => {
                // TODO
            }
            &_ => {
                println!("Invalid plugin action `{}`", action);
            }
        }
    }
}
