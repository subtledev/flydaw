
use std::sync::{Arc, Mutex};

use crate::Transport;


use crate::gui::cmd::handler::CommandHandler;

pub struct TransportCommandHandler {
    transport: Arc<Mutex<Transport>>
}

impl CommandHandler for TransportCommandHandler {
    type ConstructorArgs = (Arc<Mutex<Transport>>);

    fn construct(args: (Arc<Mutex<Transport>>)) -> TransportCommandHandler {
        let (transport) = args;
        TransportCommandHandler {
            transport
        }
    }


    fn handle(&mut self, json_value: serde_json::Value) {
        let action = json_value.get("action").and_then(|value| value.as_str())
            .map(String::from).unwrap_or_else(|| String::new());

        match action.as_str() {
            "play" => {
                self.transport.lock().unwrap().play();
            }
            "pause" => {
                self.transport.lock().unwrap().pause();
            }
            "stop" => {
                self.transport.lock().unwrap().stop();
            }
            "loop" => {
                let loop_start = json_value
                    .get("start")
                    .and_then(serde_json::Value::as_u64)
                    .map(|v| v as usize)
                    .unwrap_or_default();
                let loop_end = json_value
                    .get("end")
                    .and_then(serde_json::Value::as_u64)
                    .map(|v| v as usize)
                    .unwrap_or_default();
                self.transport.lock().unwrap().loop_samples(loop_start, loop_end);
            }
            "end-loop" => {
                self.transport.lock().unwrap().end_loop();
            }
            "position" => {
                // TODO
            }
            &_ => {
                println!("Invalid transport action `{}`", action);
            }
        }
    }
}
