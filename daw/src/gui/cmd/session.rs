
use std::sync::{Arc, Mutex};
use tokio::task;

use crate::SessionConfig;
use crate::GUIProcess;

use serde_json::json;

use crate::gui::cmd::handler::CommandHandler;

pub struct SessionCommandHandler {
    session: Arc<Mutex<SessionConfig>>,
    gui: Arc<Mutex<GUIProcess>>
}

impl CommandHandler for SessionCommandHandler {
    type ConstructorArgs = (Arc<Mutex<SessionConfig>>, Arc<Mutex<GUIProcess>>);

    fn construct(args: (Arc<Mutex<SessionConfig>>, Arc<Mutex<GUIProcess>>)) -> SessionCommandHandler {
        let (session, gui) = args;
        SessionCommandHandler {
            session,
            gui
        }
    }


    fn handle(&mut self, json_value: serde_json::Value) {
        let action = json_value.get("action").and_then(|value| value.as_str())
            .map(String::from).unwrap_or_else(|| String::new());

        let mut gui = self.gui.lock().unwrap();

        match action.as_str() {
            "bpm" => {
                let bpm = json_value
                    .get("value")
                    .and_then(serde_json::Value::as_f64)
                    .map(|v| v as f32)
                    .unwrap_or_default();

                self.session.lock().unwrap().bpm = bpm;
                gui.send(json!({
                    "command": "session",
                    "bpm": format!("{:.2}", bpm)
                }));
            }
            "time_signature" => {
                let numerator_value = json_value
                    .get("numerator")
                    .and_then(serde_json::Value::as_u64)
                    .map(|v| v as usize)
                    .unwrap_or_default();
                let denominator_value = json_value
                    .get("denominator")
                    .and_then(serde_json::Value::as_u64)
                    .map(|v| v as usize)
                    .unwrap_or_default();
                self.session.lock().unwrap().time_signature = (numerator_value, denominator_value);
                gui.send(json!({
                    "command": "session",
                    "time_signature": {
                        "numerator": numerator_value,
                        "denominator": denominator_value
                    }
                }));
            }
            "metronome" => {
                let state = json_value
                    .get("state")
                    .and_then(|value| value.as_bool())
                    .unwrap_or(false);
                self.session.lock().unwrap().metronome_on = state;
                gui.send(json!({
                    "command": "session",
                    "metronome": {
                        "state": state
                    }
                }));
            }
            "load" => {
                let session_path = json_value.get("full_path")
                    .and_then(|value| value.as_str())
                    .map(String::from)
                    .unwrap_or_else(|| String::new());

                let mut sess_cfg_mut = self.session.lock().unwrap();
                if let Some(session_arc) = sess_cfg_mut.session.clone() {
                    println!("CALL LOAD");
                    drop(sess_cfg_mut);

                    tokio::spawn(async move {
                        session_arc.lock().unwrap().load(session_path.as_str());
                    });

                } else {
                    println!("DON'T CALL");
                }
                
            }
            "save" => {
                let mut sess_cfg_mut = self.session.lock().unwrap();
                if let Some(session_arc) = sess_cfg_mut.session.clone() {
                    drop(sess_cfg_mut);

                    tokio::spawn(async move {
                        session_arc.lock().unwrap().save(None);
                    });

                }
            }
            "save_as" => {
                let session_path = json_value.get("full_path")
                    .and_then(|value| value.as_str())
                    .map(String::from)
                    .unwrap_or_else(|| String::new());

                let mut sess_cfg_mut = self.session.lock().unwrap();
                if let Some(session_arc) = sess_cfg_mut.session.clone() {
                    drop(sess_cfg_mut);

                    tokio::spawn(async move {
                        session_arc.lock().unwrap().save(Some(session_path.as_str()));
                    });

                }
            }
            &_ => {
                println!("Invalid session action `{}`", action);
            }
        }
    }
}
