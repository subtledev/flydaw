
mod jack {
    pub mod client;
    pub mod queue {
        pub mod audio {
            pub mod playback;
            pub mod recording;
        }
        pub mod midi {
            pub mod playback;
            pub mod recording;
        }
    }
}

mod mixer {
    pub mod mixer;
    pub mod track;
    pub mod audio {
        pub mod track;
        pub mod sample_cut;
    }
    pub mod midi {
        pub mod track;
        pub mod sequence;
    }
}

mod session {
    pub mod config;
    pub mod session;
}
mod transport;
mod metronome;
//mod gui;
//
mod gui {
    pub mod process;
    pub mod cmd {
        pub mod handler;
        pub mod cut;
        pub mod mixer;
        pub mod plugin;
        pub mod process;
        pub mod session;
        pub mod transport;
    }
}

mod plugins {
   pub mod mg;
   pub mod lv2 {
       pub mod host;
   }
}

mod globals {
    pub const QUEUE_LENGTH: usize = 4;
}

use std::io;


use jack::client::JackClient;

use mixer::mixer::Mixer;
use gui::process::GUIProcess;
use transport::Transport;
use plugins::mg::PluginsManager;
use metronome::Metronome;

use std::sync::{Arc, Mutex};


use session::config::SessionConfig;
use session::session::Session;

#[tokio::main]
async fn main() {
    let session_config = Arc::new(Mutex::new(SessionConfig::new(JackClient::sample_rate(), JackClient::buffer_size())));
    let transport = Arc::new(Mutex::new(Transport::new(Arc::clone(&session_config))));
//    transport.lock().unwrap().loop_samples(0, 100240);

    let jack_client = Arc::new(Mutex::new(JackClient::new(
        Arc::clone(&transport),
        Arc::clone(&session_config),
    )));
    let track_mixer = Arc::new(Mutex::new(Mixer::new(
        Arc::clone(&jack_client),
        Arc::clone(&session_config)
    )));

    let metronome = Arc::new(Mutex::new(Metronome::new(
        Arc::clone(&jack_client),
        Arc::clone(&session_config)
    )));

    println!("INIT GUI");

    let mut new_gui = GUIProcess::new();
    let gui = Arc::new(Mutex::new(new_gui));
    gui.lock().unwrap().initialize_communications(Arc::clone(&track_mixer), Arc::clone(&transport), Arc::clone(&gui)).await;
    
//    let gui = Arc::new(Mutex::new(GUI::new(Arc::clone(&track_mixer), Arc::clone(&transport)).await));
    track_mixer.lock().unwrap().set_gui(Arc::clone(&gui));
    transport.lock().unwrap().set_gui(Arc::clone(&gui));
    println!("GUI RDY");

    let session = Arc::new(Mutex::new(Session::new(
        Arc::clone(&session_config),
        Arc::clone(&track_mixer),
        Arc::clone(&gui)
    )));
    session_config.lock().unwrap().set_session(Arc::clone(&session));
    println!("SESSION RDY");



    let transport_clone = Arc::clone(&transport);
//    thread::sleep(Duration::from_secs(5));
    let _playback_loop = tokio::spawn(async move {
        loop {
            let mut transport_ref = transport_clone.lock().unwrap();
            let transport_position = transport_ref.get_position();
            let is_playing = transport_ref.is_playing;
            drop(transport_ref);

            metronome.lock().unwrap().update(transport_position);

            if !is_playing {
                track_mixer.lock().unwrap().stop(transport_position);
            } else {
                track_mixer.lock().unwrap().update(transport_position);
            }

        }
    });

    let plugins_mg = PluginsManager::new();


//    session.lock().unwrap().load("/home/qualphey/FlyDaw/test-session");
    // Wait for user input to quit
    println!("Press enter/return to quit...");
    let mut user_input = String::new();
    io::stdin().read_line(&mut user_input).ok();


    let jc_mutex = jack_client.as_ref();
    let mut jack_client = jc_mutex.lock().unwrap();
    jack_client.deactivate();
}

