
use tokio::io::{AsyncBufReadExt, AsyncWriteExt, BufReader, BufWriter};
use tokio::process::{Command};

use std::thread;

use std::process::Stdio;
use std::time::Duration;

use serde_json::{Value};

use std::sync::{Arc, Mutex};

use crate::Mixer;
use crate::Transport;

pub struct GUI {
//    child_process: Option<Child>,
    output_queue: Arc<Mutex<Vec<String>>>,
//    mixer: Arc<Mutex<Mixer>>
}

impl GUI {
    pub async fn new(_mixer: Arc<Mutex<Mixer>>, transport: Arc<Mutex<Transport>>) -> GUI {

        let mixer = Arc::clone(&_mixer);
        // Spawn the child application
//          let mut child_process = Command::new("/home/qualphey/Development/app/jack-daw/tauri-app/src-tauri/target/release/tauri-app")
        let mut child_process = Command::new("/home/qualphey/Development/app/jack-daw/tauri-app/run-dev.sh")
            .stdin(Stdio::piped())
            .stdout(Stdio::piped())
            .spawn()
            .expect("Failed to spawn child process");

        let output_queue: Arc<Mutex<Vec<String>>> = Arc::new(Mutex::new(Vec::new()));
        let output_queue_reader = output_queue.clone();

        let ready: Arc<Mutex<bool>> = Arc::new(Mutex::new(false));
        let readyc: Arc<Mutex<bool>> = Arc::clone(&ready);

        if let (Some(child_stdout), Some(child_stdin)) = (child_process.stdout.take(), child_process.stdin.take()) {
            let _child_stdout_reader = tokio::spawn(async move {
                let mut child_reader = BufReader::new(child_stdout).lines();
                while let Some(line) = child_reader.next_line().await.unwrap() {
                    let result: Result<Value, serde_json::Error> = serde_json::from_str(line.as_str());
                    match result {
                        Ok(json_value) => {
                            if let Some(cmd) = json_value.get("command") {
                                println!("Command: {}", cmd);
                                if cmd == "ready" {
                                    let mut rval = ready.lock().unwrap();
                                    *rval = true;
                                } else if cmd == "tracks" {

                                    if let Some(create) = json_value.get("create") {
                                        println!("Create: {}", create);
                                        if create == "audio" {
                                            mixer.lock().unwrap().add_audio_track(None);
                                        } else if create == "midi" {
                                            mixer.lock().unwrap().add_midi_track(None);
                                        }
                                    } else if let Some(import_files) = json_value.get("import_files") {
                                        let track = import_files
                                            .get("track")
                                            .and_then(|value| value.as_str())
                                            .map(String::from)
                                            .unwrap_or_else(|| String::new());

                                        println!("trackname: {}", track);

                                        let files = import_files
                                            .get("files")
                                            .and_then(|value| value.as_array())
                                            .map(|array| array.iter().filter_map(|v| v.as_str().map(String::from)).collect())
                                            .unwrap_or_else(|| Vec::new());

                                        mixer.lock().unwrap().import_files(track, files, 0.0);
                                    } else if let Some(set_state) = json_value.get("set_state") {
                                        let track_name = set_state
                                            .get("track_name")
                                            .and_then(|value| value.as_str())
                                            .map(String::from)
                                            .unwrap_or_else(|| String::new());
                                        let which = set_state
                                            .get("which")
                                            .and_then(|value| value.as_str())
                                            .map(String::from)
                                            .unwrap_or_else(|| String::new());
                                        let state = set_state
                                            .get("state")
                                            .and_then(|value| value.as_bool())
                                            .unwrap_or(false);
                                        mixer.lock().unwrap().set_track_state(track_name, which, state);
                                    } else if let Some(move_sample_cut) = json_value.get("move_sample_cut") {
                                        let track_name = move_sample_cut
                                            .get("track")
                                            .and_then(|value| value.as_str())
                                            .map(String::from)
                                            .unwrap_or_else(|| String::new());
                                        let cut_id = move_sample_cut
                                            .get("cut_id")
                                            .and_then(Value::as_u64)
                                            .map(|v| v as usize)
                                            .unwrap_or_default();
                                        let position = move_sample_cut
                                            .get("position")
                                            .and_then(Value::as_u64)
                                            .map(|v| v as usize)
                                            .unwrap_or_default();

                                        mixer.lock().unwrap().move_sample_cut(track_name, cut_id, position);
                                    }
                                } else if cmd == "transport" {
                                    if let Some(_val) = json_value.get("play") {
                                        transport.lock().unwrap().play();
                                    } else if let Some(_val) =  json_value.get("pause") {
                                        transport.lock().unwrap().pause();
                                    } else if let Some(_val) =  json_value.get("stop") {
                                        transport.lock().unwrap().stop();
                                    }
                                }
                            }
                        }
                        Err(e) => {
                            println!("Error parsing JSON: {}", e);
                        }
                    }
                }
            });
            
            let _child_stdin_writer = tokio::spawn(async move {
                let output_queue = Arc::clone(&output_queue_reader);

                let mut child_writer = BufWriter::new(child_stdin);

                loop {
                    if output_queue.lock().unwrap().len() > 0 {
                        let values_to_process: Vec<_> = output_queue.lock().unwrap().iter().cloned().collect();
                        output_queue.lock().unwrap().clear(); // Clear the queue
                        
                        for first_value in values_to_process {
                            println!("Processing value: {}", first_value);
                            child_writer.write_all(first_value.as_bytes()).await.expect("Failed to write to child stdin");
                            child_writer.flush().await.expect("Failed to flush child stdin");
                        }
                    }
                }
            });

            // Write to child's stdin
      //      child_stdin.write_all(b"Hello from the parent!").await.expect("Failed to write to child stdin");

            // Spawn a task for writing to the child's stdin

/*
            // Wait for the child process to finish
            let status = child_process.wait().await.expect("Failed to wait for child process");
            println!("Child process exited with: {:?}", status);

            // Wait for the child_stdout_reader task to finish
            child_stdout_reader.await.expect("Failed to await child_stdout_reader");*/
        }

        let wait_ready = tokio::spawn(async move {
            while !(*readyc.lock().unwrap()) {
                thread::sleep(Duration::from_millis(333));
            }
        });
        wait_ready.await.expect("Failed to start GUI.");

        GUI {
//            child_process: Some(child_process),
            output_queue: output_queue,
//            mixer: _mixer
        }
    }

    pub fn send<T>(&mut self, data: T) where T: serde::Serialize {
        let json_value = serde_json::to_value(data).expect("Failed to serialize to JSON");
        let mut output_queue = self.output_queue.lock().unwrap();
        output_queue.push(format!("{}\n", json_value.to_string()));
    }
}
