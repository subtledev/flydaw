
pub struct MidiRecordingQueue {
    queue: Vec<(u32, Vec<u8>)>
}

impl MidiRecordingQueue {
    pub fn new() -> MidiRecordingQueue {
        MidiRecordingQueue {
            queue: Vec::new()
        }
    }

    
    pub fn input_events(
        &mut self,
        position: usize,
        input_port: &jack::Port<jack::MidiIn>,
        ps: &jack::ProcessScope
    ) {
        for event in input_port.iter(ps) {
            println!("Received MIDI message: {:?}", event);
            self.queue.push(
                (
                    event.time + position as u32,
                    event.bytes.to_vec()
                )
            );
        }
    }

    pub fn next_event(&mut self) -> Option<(u32, Vec<u8>)> {
        if self.queue.len() > 0 {
            Some(self.queue.remove(0))
        } else {
            None
        }
    }
}
