


pub struct MidiPlaybackQueue {
    queue: Vec<(u32, [u8; 3])>,
    drain: usize
}

impl MidiPlaybackQueue {
    pub fn new() -> MidiPlaybackQueue {
        MidiPlaybackQueue {
            queue: Vec::new(),
            drain: 0
        }
    }


    pub fn extend(&mut self, events: Vec<(u32, [u8; 3])>) {
       self.queue.extend(events); 
    }

    pub fn output_midi(
        &mut self,
        position: usize,
        frame_size: usize,
        output_port: &mut jack::Port<jack::MidiOut>,
        ps: &jack::ProcessScope
    ) {
        if self.queue.len() > 0 {
            /*if self.drain > 0 {
                self.queue.drain(0..self.drain);
                self.drain = 0;
            }*/

            let mut events_played = 0;

            let mut midi_out = output_port.writer(ps);

            while let Some((time, bytes)) = self.queue.get(self.drain) {
                let rel_time = *time as usize - position;
                if rel_time <= frame_size {
                    let msg = jack::RawMidi {
                        time: rel_time as u32,
                        bytes
                    };
                    println!("MIDI OUT; rel_time: {}; msg: {:?}", rel_time, msg);
                    midi_out.write(&msg);
                    self.drain += 1;
                } else {
                    break;
                }
            }

// TODO better track the number of events played and remove 'em on e separate thread
 
        }
    }
}

