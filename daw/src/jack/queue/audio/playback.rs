
use crate::globals;

enum SamplePlaybackQueue
{
    Frame16(PlaybackQueueImpl<16>),
    Frame32(PlaybackQueueImpl<32>),
    Frame64(PlaybackQueueImpl<64>),
    Frame128(PlaybackQueueImpl<128>),
    Frame256(PlaybackQueueImpl<256>),
    Frame512(PlaybackQueueImpl<512>),
    Frame1024(PlaybackQueueImpl<1024>),
    Frame2048(PlaybackQueueImpl<2048>),
    Frame4096(PlaybackQueueImpl<4096>),
    Frame8192(PlaybackQueueImpl<8192>),
}

impl SamplePlaybackQueue
{
    fn frame_size(&self) -> usize {
        match self {
            SamplePlaybackQueue::Frame16(frame) => frame.frame_size(),
            SamplePlaybackQueue::Frame32(frame) => frame.frame_size(),
            SamplePlaybackQueue::Frame64(frame) => frame.frame_size(),
            SamplePlaybackQueue::Frame128(frame) => frame.frame_size(),
            SamplePlaybackQueue::Frame256(frame) => frame.frame_size(),
            SamplePlaybackQueue::Frame512(frame) => frame.frame_size(),
            SamplePlaybackQueue::Frame1024(frame) => frame.frame_size(),
            SamplePlaybackQueue::Frame2048(frame) => frame.frame_size(),
            SamplePlaybackQueue::Frame4096(frame) => frame.frame_size(),
            SamplePlaybackQueue::Frame8192(frame) => frame.frame_size(),
        }
    }

    fn update_queue(&mut self, next_frame: [Vec<f32>; 2]) {
        match self {
            SamplePlaybackQueue::Frame16(frame) => frame.update_queue(next_frame),
            SamplePlaybackQueue::Frame32(frame) => frame.update_queue(next_frame),
            SamplePlaybackQueue::Frame64(frame) => frame.update_queue(next_frame),
            SamplePlaybackQueue::Frame128(frame) => frame.update_queue(next_frame),
            SamplePlaybackQueue::Frame256(frame) => frame.update_queue(next_frame),
            SamplePlaybackQueue::Frame512(frame) => frame.update_queue(next_frame),
            SamplePlaybackQueue::Frame1024(frame) => frame.update_queue(next_frame),
            SamplePlaybackQueue::Frame2048(frame) => frame.update_queue(next_frame),
            SamplePlaybackQueue::Frame4096(frame) => frame.update_queue(next_frame),
            SamplePlaybackQueue::Frame8192(frame) => frame.update_queue(next_frame),
        }
    }

    fn output_audio(&self, output_buffer: &mut [f32], ch: usize) {
        match self {
            SamplePlaybackQueue::Frame16(frame) => frame.output_audio(output_buffer, ch),
            SamplePlaybackQueue::Frame32(frame) => frame.output_audio(output_buffer, ch),
            SamplePlaybackQueue::Frame64(frame) => frame.output_audio(output_buffer, ch),
            SamplePlaybackQueue::Frame128(frame) => frame.output_audio(output_buffer, ch),
            SamplePlaybackQueue::Frame256(frame) => frame.output_audio(output_buffer, ch),
            SamplePlaybackQueue::Frame512(frame) => frame.output_audio(output_buffer, ch),
            SamplePlaybackQueue::Frame1024(frame) => frame.output_audio(output_buffer, ch),
            SamplePlaybackQueue::Frame2048(frame) => frame.output_audio(output_buffer, ch),
            SamplePlaybackQueue::Frame4096(frame) => frame.output_audio(output_buffer, ch),
            SamplePlaybackQueue::Frame8192(frame) => frame.output_audio(output_buffer, ch),
        }
    }
}



struct PlaybackQueueImpl<const N: usize>
{
    queue: Box<[[[f32; N]; 2]; globals::QUEUE_LENGTH]>,
}

impl<const N: usize> PlaybackQueueImpl<N>
{
    fn new() -> Self
    {
        PlaybackQueueImpl {
            queue: Box::new([[[0.0; N]; 2]; globals::QUEUE_LENGTH]),
        }
    }

    fn frame_size(&self) -> usize {
        N
    }

    fn update_queue(&mut self, next_frame: [Vec<f32>; 2]) {
        for i in 0..globals::QUEUE_LENGTH - 1 {
            self.queue[i] = self.queue[i + 1];
        }
        for ch in 0..2 {
            self.queue[globals::QUEUE_LENGTH - 1][ch].clone_from_slice(next_frame[ch].as_slice());
        }
    }

    fn output_audio(&self, output_buffer: &mut [f32], ch: usize) {
//        println!("{:?}", output_buffer);
//        println!("OUTPUT NEXT FRAME");
        output_buffer.clone_from_slice(&self.queue[1][ch]);
    }
}

// Your main struct
pub struct AudioPlaybackQueue
{
    samples: SamplePlaybackQueue,
}

impl AudioPlaybackQueue
{
    pub fn new() -> AudioPlaybackQueue {
        AudioPlaybackQueue {
            samples: SamplePlaybackQueue::Frame128(PlaybackQueueImpl::new()),
        }
    }

    pub fn set_frame_size(&mut self, frame_size: usize) {
        self.samples = match frame_size {
            16 => SamplePlaybackQueue::Frame16(PlaybackQueueImpl::new()),
            32 => SamplePlaybackQueue::Frame32(PlaybackQueueImpl::new()),
            64 => SamplePlaybackQueue::Frame64(PlaybackQueueImpl::new()),
            128 => SamplePlaybackQueue::Frame128(PlaybackQueueImpl::new()),
            256 => SamplePlaybackQueue::Frame256(PlaybackQueueImpl::new()),
            512 => SamplePlaybackQueue::Frame512(PlaybackQueueImpl::new()),
            1024 => SamplePlaybackQueue::Frame1024(PlaybackQueueImpl::new()),
            2048 => SamplePlaybackQueue::Frame2048(PlaybackQueueImpl::new()),
            4096 => SamplePlaybackQueue::Frame4096(PlaybackQueueImpl::new()),
            8192 => SamplePlaybackQueue::Frame8192(PlaybackQueueImpl::new()),
            _ => {
                // Handle the case for unknown frame size
                println!("Unknown frame size");
                SamplePlaybackQueue::Frame16(PlaybackQueueImpl::new())
            }
        };
    }

    pub fn output_audio(&self, output_buffer: &mut [f32], ch: usize) {
        self.samples.output_audio(output_buffer, ch);
    }

    pub fn progress(&mut self, next_frame: [Vec<f32>; 2]) {
        self.samples.update_queue(next_frame);
    }
}
