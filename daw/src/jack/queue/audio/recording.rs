
enum SampleRecordingQueue
{
    Frame16(RecordingQueueImpl<16>),
    Frame32(RecordingQueueImpl<32>),
    Frame64(RecordingQueueImpl<64>),
    Frame128(RecordingQueueImpl<128>),
    Frame256(RecordingQueueImpl<256>),
    Frame512(RecordingQueueImpl<512>),
    Frame1024(RecordingQueueImpl<1024>),
    Frame2048(RecordingQueueImpl<2048>),
    Frame4096(RecordingQueueImpl<4096>),
    Frame8192(RecordingQueueImpl<8192>),
}

impl SampleRecordingQueue
{
    fn frame_size(&self) -> usize {
        match self {
            SampleRecordingQueue::Frame16(frame) => frame.frame_size(),
            SampleRecordingQueue::Frame32(frame) => frame.frame_size(),
            SampleRecordingQueue::Frame64(frame) => frame.frame_size(),
            SampleRecordingQueue::Frame128(frame) => frame.frame_size(),
            SampleRecordingQueue::Frame256(frame) => frame.frame_size(),
            SampleRecordingQueue::Frame512(frame) => frame.frame_size(),
            SampleRecordingQueue::Frame1024(frame) => frame.frame_size(),
            SampleRecordingQueue::Frame2048(frame) => frame.frame_size(),
            SampleRecordingQueue::Frame4096(frame) => frame.frame_size(),
            SampleRecordingQueue::Frame8192(frame) => frame.frame_size(),
        }
    }

    fn write_next_frame(
        &mut self,
        writer: &mut hound::WavWriter<std::io::BufWriter<std::fs::File>>
    ) {
        match self {
            SampleRecordingQueue::Frame16(frame) => frame.write_next_frame(writer),
            SampleRecordingQueue::Frame32(frame) => frame.write_next_frame(writer),
            SampleRecordingQueue::Frame64(frame) => frame.write_next_frame(writer),
            SampleRecordingQueue::Frame128(frame) => frame.write_next_frame(writer),
            SampleRecordingQueue::Frame256(frame) => frame.write_next_frame(writer),
            SampleRecordingQueue::Frame512(frame) => frame.write_next_frame(writer),
            SampleRecordingQueue::Frame1024(frame) => frame.write_next_frame(writer),
            SampleRecordingQueue::Frame2048(frame) => frame.write_next_frame(writer),
            SampleRecordingQueue::Frame4096(frame) => frame.write_next_frame(writer),
            SampleRecordingQueue::Frame8192(frame) => frame.write_next_frame(writer),
        }
    }

    fn input_audio(
        &mut self,
        input_ports: &Vec<jack::Port<jack::AudioIn>>,
        ps: &jack::ProcessScope
    ) {
        match self {
            SampleRecordingQueue::Frame16(frame) => frame.input_audio(input_ports, ps),
            SampleRecordingQueue::Frame32(frame) => frame.input_audio(input_ports, ps),
            SampleRecordingQueue::Frame64(frame) => frame.input_audio(input_ports, ps),
            SampleRecordingQueue::Frame128(frame) => frame.input_audio(input_ports, ps),
            SampleRecordingQueue::Frame256(frame) => frame.input_audio(input_ports, ps),
            SampleRecordingQueue::Frame512(frame) => frame.input_audio(input_ports, ps),
            SampleRecordingQueue::Frame1024(frame) => frame.input_audio(input_ports, ps),
            SampleRecordingQueue::Frame2048(frame) => frame.input_audio(input_ports, ps),
            SampleRecordingQueue::Frame4096(frame) => frame.input_audio(input_ports, ps),
            SampleRecordingQueue::Frame8192(frame) => frame.input_audio(input_ports, ps),
        }
    }
}

struct RecordingQueueImpl<const N: usize>
{
    queue: Vec<[[f32; N]; 2]>,
}

impl<const N: usize> RecordingQueueImpl<N>
{
    fn new() -> Self
    {
        RecordingQueueImpl {
            queue: Vec::new(),
        }
    }

    fn frame_size(&self) -> usize {
        N
    }

    fn write_next_frame(
        &mut self,
        writer: &mut hound::WavWriter<std::io::BufWriter<std::fs::File>>
    ) {
        if self.queue.len() > 0 {
            if let next_frame = self.queue.remove(0) {
                for s in 0..N {
                    for c in 0..2 {
                        let sample = (next_frame[c][s] * i16::MAX as f32) as i16;
                        writer.write_sample(sample);
                    }
                }
            }
        }
        return;
    }

    fn input_audio(
        &mut self,
        input_ports: &Vec<jack::Port<jack::AudioIn>>,
        ps: &jack::ProcessScope
    ) {
        let mut stereo_frame: [[f32; N]; 2] = [[0.0; N]; 2];

        for c in 0..2 {
            let input_buffer = input_ports[c].as_slice(ps);
            match input_buffer.try_into() {
                Ok(slice_array) => {
                    stereo_frame[c] = slice_array;
                }
                Err(err) => {
                    eprintln!(
                        "Error converting input_buffer to slice_array: {:?}",
                        err
                    );
                }
            }
        }

        self.queue.push(stereo_frame);
        return;
    }
}

pub struct AudioRecordingQueue {
    samples: SampleRecordingQueue
}

impl AudioRecordingQueue {
    pub fn new() -> AudioRecordingQueue {
        AudioRecordingQueue {
            samples: SampleRecordingQueue::Frame128(RecordingQueueImpl::new()),
        }
    }

    pub fn set_frame_size(&mut self, frame_size: usize) {
        self.samples = match frame_size {
            16 => SampleRecordingQueue::Frame16(RecordingQueueImpl::new()),
            32 => SampleRecordingQueue::Frame32(RecordingQueueImpl::new()),
            64 => SampleRecordingQueue::Frame64(RecordingQueueImpl::new()),
            128 => SampleRecordingQueue::Frame128(RecordingQueueImpl::new()),
            256 => SampleRecordingQueue::Frame256(RecordingQueueImpl::new()),
            512 => SampleRecordingQueue::Frame512(RecordingQueueImpl::new()),
            1024 => SampleRecordingQueue::Frame1024(RecordingQueueImpl::new()),
            2048 => SampleRecordingQueue::Frame2048(RecordingQueueImpl::new()),
            4096 => SampleRecordingQueue::Frame4096(RecordingQueueImpl::new()),
            8192 => SampleRecordingQueue::Frame8192(RecordingQueueImpl::new()),
            _ => {
                // Handle the case for unknown frame size
                println!("{}", format!(
                    "Error: unsupported frame size: {}",
                    frame_size
                ));
                SampleRecordingQueue::Frame128(RecordingQueueImpl::new())
            }
        };
    }

    pub fn input_audio(
        &mut self,
        input_ports: &Vec<jack::Port<jack::AudioIn>>,
        ps: &jack::ProcessScope
    ) {
        println!("ADD AUDIO FROM INPUT TO QUEUE");
        self.samples.input_audio(input_ports, ps);

    }

    pub fn write_next_frame(
        &mut self,
        writer: &mut hound::WavWriter<std::io::BufWriter<std::fs::File>>
    ) {
        println!("WRITE FRAME TO FILE");
        self.samples.write_next_frame(writer);
    }
}
