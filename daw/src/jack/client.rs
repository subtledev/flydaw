use std::collections::HashMap;
use std::sync::{Arc, Mutex};


use crate::jack::queue::audio::playback::AudioPlaybackQueue;
use crate::jack::queue::audio::recording::AudioRecordingQueue;
use crate::jack::queue::midi::playback::MidiPlaybackQueue;
use crate::jack::queue::midi::recording::MidiRecordingQueue;
use crate::transport::Transport;
use crate::session::config::SessionConfig;

struct FlyProcessHandler {
    audio_tracks: Arc<Mutex<Vec<String>>>,
    at_input_ports: Arc<Mutex<HashMap<String, Vec<jack::Port<jack::AudioIn>>>>>,
    at_output_ports: Arc<Mutex<HashMap<String, Vec<jack::Port<jack::AudioOut>>>>>,
    at_playback_queue: Arc<Mutex<HashMap<String, Arc<Mutex<AudioPlaybackQueue>>>>>,
    at_recordin_queue: Arc<Mutex<HashMap<String, Arc<Mutex<AudioRecordingQueue>>>>>,
    at_cur_frame: Arc<Mutex<HashMap<String, usize>>>,
    at_control_states: Arc<Mutex<HashMap<String, Arc<Mutex<HashMap<String, bool>>>>>>,

    midi_tracks: Arc<Mutex<Vec<String>>>,
    mt_input_ports: Arc<Mutex<HashMap<String, jack::Port<jack::MidiIn>>>>,
    mt_output_ports: Arc<Mutex<HashMap<String, jack::Port<jack::MidiOut>>>>,
    mt_control_states: Arc<Mutex<HashMap<String, Arc<Mutex<HashMap<String, bool>>>>>>,
    mt_playback_queue: Arc<Mutex<HashMap<String, Arc<Mutex<MidiPlaybackQueue>>>>>,
    mt_recordin_queue: Arc<Mutex<HashMap<String, Arc<Mutex<MidiRecordingQueue>>>>>,

    master_in_left: jack::Port<jack::AudioIn>,
    master_in_right: jack::Port<jack::AudioIn>,
    master_out_left: jack::Port<jack::AudioOut>,
    master_out_right: jack::Port<jack::AudioOut>,

    transport: Arc<Mutex<Transport>>
}

impl jack::ProcessHandler for FlyProcessHandler {
    fn process(&mut self, _: &jack::Client, ps: &jack::ProcessScope) -> jack::Control {
        let mut transport = self.transport.lock().unwrap();
        let is_playing: bool = transport.is_playing;
        let mut position: usize = 0;
        if transport.is_playing {
            position = transport.next_frame();
        }
        let mut session_mut = transport.session.lock().unwrap();
        let frame_size = session_mut.frame_size;
        let metronome_on = session_mut.metronome_on;
        drop(session_mut);
        drop(transport);


        let audio_tracks = self.audio_tracks.lock().unwrap();
        let at_len = audio_tracks.len();
        drop(audio_tracks);
        for t in 0..at_len {
            let audio_tracks = self.audio_tracks.lock().unwrap();
            let track_name: &String = &audio_tracks[t].clone();
            drop(audio_tracks);

            if track_name == "metronome" {
                if is_playing && metronome_on {
                    let playback_queue_hm = self.at_playback_queue.lock().unwrap();
                    let mut playback_queue = playback_queue_hm.get(track_name).unwrap().lock().unwrap();

                    let mut output_ports_hm = self.at_output_ports.lock().unwrap();
                    let output_ports: &mut Vec<jack::Port<jack::AudioOut>> =
                        output_ports_hm.get_mut(track_name).unwrap();

                    for c in 0..output_ports.len() {
                        playback_queue.output_audio(output_ports[c].as_mut_slice(ps), c);
                    }
                }
            } else {
                if is_playing {
                    let control_states_hm = self.at_control_states.lock().unwrap();
                    let control_states = control_states_hm.get(track_name).unwrap().lock().unwrap();
                    let playback_queue_hm = self.at_playback_queue.lock().unwrap();
                    let mut playback_queue = playback_queue_hm.get(track_name).unwrap().lock().unwrap();

                    if *control_states.get("record").unwrap_or(&false) {
                        let recordin_queue_hm = self.at_recordin_queue.lock().unwrap();
                        let mut recordin_queue = recordin_queue_hm.get(track_name).unwrap().lock().unwrap();

                        let input_ports_hm = self.at_input_ports.lock().unwrap();
                        let input_ports = input_ports_hm.get(track_name).unwrap();

                        recordin_queue.input_audio(input_ports, ps);
                    } else if !*control_states.get("mute").unwrap_or(&false) {
                        let mut output_ports_hm = self.at_output_ports.lock().unwrap();
                        let output_ports: &mut Vec<jack::Port<jack::AudioOut>> =
                            output_ports_hm.get_mut(track_name).unwrap();

                        for c in 0..output_ports.len() {
                            playback_queue.output_audio(output_ports[c].as_mut_slice(ps), c);
                        }
                    }
                } else {
                    let mut output_ports_hm = self.at_output_ports.lock().unwrap();
                    let output_ports: &mut Vec<jack::Port<jack::AudioOut>> = output_ports_hm.get_mut(track_name).unwrap();
                    let input_ports_hm = self.at_input_ports.lock().unwrap();
                    let input_ports = input_ports_hm.get(track_name).unwrap();
                    for c in 0..output_ports.len() {
                        let output_buffer = output_ports[c].as_mut_slice(ps);
                        let input_buffer = input_ports[c].as_slice(ps);
                        output_buffer.clone_from_slice(input_buffer);
                    }
                }
            }
        }

        let midi_tracks = self.midi_tracks.lock().unwrap();
        let mt_len = midi_tracks.len();
        drop(midi_tracks);
        for t in 0..mt_len {
            let midi_tracks = self.midi_tracks.lock().unwrap();
            let track_name: &String = &midi_tracks[t].clone();
            drop(midi_tracks);

            if is_playing {
                let control_states_hm = self.mt_control_states.lock().unwrap();
                let control_states = control_states_hm.get(track_name).unwrap().lock().unwrap();

                if *control_states.get("record").unwrap_or(&false) {
                    let input_ports_hm = self.mt_input_ports.lock().unwrap();
                    let input_port = input_ports_hm.get(track_name).unwrap();

                    let recordin_queue_hm = self.mt_recordin_queue.lock().unwrap();
                    let mut recordin_queue = recordin_queue_hm.get(track_name).unwrap().lock().unwrap();

                    recordin_queue.input_events(position, input_port, ps);

               } else {
                   let mut output_ports_hm = self.mt_output_ports.lock().unwrap();
                   let mut output_port: &mut jack::Port<jack::MidiOut> = output_ports_hm.get_mut(track_name).unwrap();

                   let playback_queue_hm = self.mt_playback_queue.lock().unwrap();
                   let mut playback_queue = playback_queue_hm.get(track_name).unwrap().lock().unwrap();

                   playback_queue.output_midi(position, frame_size, output_port, ps);

               }
            } else {
                //not playing
            }
        }


        self.master_out_left.as_mut_slice(ps).clone_from_slice(self.master_in_left.as_slice(ps));
        self.master_out_right.as_mut_slice(ps).clone_from_slice(self.master_in_right.as_slice(ps));

        jack::Control::Continue
    }
}

impl FlyProcessHandler {

}

pub struct JackClient {
    active_client: Option<jack::AsyncClient<Notifications, FlyProcessHandler>>,

    audio_tracks: Arc<Mutex<Vec<String>>>,
    at_input_ports: Arc<Mutex<HashMap<String, Vec<jack::Port<jack::AudioIn>>>>>,
    at_output_ports: Arc<Mutex<HashMap<String, Vec<jack::Port<jack::AudioOut>>>>>,
    at_playback_queue: Arc<Mutex<HashMap<String, Arc<Mutex<AudioPlaybackQueue>>>>>,
    at_recordin_queue: Arc<Mutex<HashMap<String, Arc<Mutex<AudioRecordingQueue>>>>>,
    at_cur_frame: Arc<Mutex<HashMap<String, usize>>>,
    at_control_states: Arc<Mutex<HashMap<String, Arc<Mutex<HashMap<String, bool>>>>>>,

    midi_tracks: Arc<Mutex<Vec<String>>>,
    mt_input_ports: Arc<Mutex<HashMap<String, jack::Port<jack::MidiIn>>>>,
    mt_output_ports: Arc<Mutex<HashMap<String, jack::Port<jack::MidiOut>>>>,
    mt_playback_queue: Arc<Mutex<HashMap<String, Arc<Mutex<MidiPlaybackQueue>>>>>,
    mt_recordin_queue: Arc<Mutex<HashMap<String, Arc<Mutex<MidiRecordingQueue>>>>>,
    mt_control_states: Arc<Mutex<HashMap<String, Arc<Mutex<HashMap<String, bool>>>>>>,
  
    pub session: Arc<Mutex<SessionConfig>>,
}



impl JackClient {

    pub fn new(transport: Arc<Mutex<Transport>>, session: Arc<Mutex<SessionConfig>>) -> JackClient {

        // Create client
        let (client, _status) =
            jack::Client::new("flydaw", jack::ClientOptions::NO_START_SERVER).unwrap();


        let master_in_left: jack::Port<jack::AudioIn> =
            client.register_port("master_in_left", jack::AudioIn::default()).unwrap();
        let master_in_right: jack::Port<jack::AudioIn> =
            client.register_port("master_in_right", jack::AudioIn::default()).unwrap();

        let master_out_left: jack::Port<jack::AudioOut> =
            client.register_port("master_out_left", jack::AudioOut::default()).unwrap();
        let master_out_right: jack::Port<jack::AudioOut> =
            client.register_port("master_out_right", jack::AudioOut::default()).unwrap();

        let audio_tracks = Arc::new(Mutex::new(Vec::new()));
        let at_input_ports =  Arc::new(Mutex::new(HashMap::new()));
        let at_output_ports =  Arc::new(Mutex::new(HashMap::new()));
        let at_playback_queue =  Arc::new(Mutex::new(HashMap::new()));
        let at_recordin_queue =  Arc::new(Mutex::new(HashMap::new()));
        let at_cur_frame =  Arc::new(Mutex::new(HashMap::new()));
        let at_control_states =  Arc::new(Mutex::new(HashMap::new()));

        let midi_tracks = Arc::new(Mutex::new(Vec::new()));
        let mt_input_ports =  Arc::new(Mutex::new(HashMap::new()));
        let mt_output_ports =  Arc::new(Mutex::new(HashMap::new()));
        let mt_playback_queue =  Arc::new(Mutex::new(HashMap::new()));
        let mt_recordin_queue =  Arc::new(Mutex::new(HashMap::new()));
        let mt_control_states =  Arc::new(Mutex::new(HashMap::new()));

        let process_handler = FlyProcessHandler {
            audio_tracks: Arc::clone(&audio_tracks),
            at_input_ports: Arc::clone(&at_input_ports),
            at_output_ports: Arc::clone(&at_output_ports),
            at_playback_queue: Arc::clone(&at_playback_queue),
            at_recordin_queue: Arc::clone(&at_recordin_queue),
            at_cur_frame: Arc::clone(&at_cur_frame),
            at_control_states: Arc::clone(&at_control_states),

            midi_tracks: Arc::clone(&midi_tracks),
            mt_input_ports: Arc::clone(&mt_input_ports),
            mt_output_ports: Arc::clone(&mt_output_ports),
            mt_playback_queue: Arc::clone(&mt_playback_queue),
            mt_recordin_queue: Arc::clone(&mt_recordin_queue),
            mt_control_states: Arc::clone(&mt_control_states),

            master_in_left,
            master_in_right,
            master_out_left,
            master_out_right,
            transport
        };

        let active_client = client.activate_async(Notifications, process_handler).unwrap();

        active_client.as_client().connect_ports_by_name(
            "flydaw:master_out_left",
            "system:playback_1"
        ).unwrap();

        active_client.as_client().connect_ports_by_name(
            "flydaw:master_out_right",
            "system:playback_2"
        ).unwrap();

        JackClient {
            active_client: Some(active_client),
            audio_tracks,
            at_input_ports,
            at_output_ports,
            at_playback_queue,
            at_recordin_queue,
            at_cur_frame,
            at_control_states,

            midi_tracks,
            mt_input_ports,
            mt_output_ports,
            mt_playback_queue,
            mt_recordin_queue,
            mt_control_states,
            session
        }
    }

    pub fn add_metronome(
        &mut self,
        playback_queue: Arc<Mutex<AudioPlaybackQueue>>
    ) {
        let mut output_ports: Vec<jack::Port<jack::AudioOut>> = Vec::new();
        output_ports.push(self.active_client.as_ref().unwrap().as_client().register_port("metro_o_l", jack::AudioOut::default()).unwrap());
        output_ports.push(self.active_client.as_ref().unwrap().as_client().register_port("metro_o_r", jack::AudioOut::default()).unwrap());

        self.active_client.as_ref().unwrap().as_client().connect_ports_by_name(
            "flydaw:metro_o_l",
            "flydaw:master_in_left"
        ).unwrap();

        self.active_client.as_ref().unwrap().as_client().connect_ports_by_name(
            "flydaw:metro_o_r",
            "flydaw:master_in_right"
        ).unwrap();

        let name = "metronome";
        self.audio_tracks.lock().unwrap().push(name.to_string());
        self.at_output_ports.lock().unwrap().insert(name.to_string(), output_ports);
        self.at_playback_queue.lock().unwrap().insert(name.to_string(), playback_queue);
    }

    pub fn add_track(
        &mut self, name: &str,
        playback_queue: Arc<Mutex<AudioPlaybackQueue>>,
        recordin_queue: Arc<Mutex<AudioRecordingQueue>>,
        control_states: Arc<Mutex<HashMap<String, bool>>>
    ) {

        let mut input_ports: Vec<jack::Port<jack::AudioIn>> = Vec::new();
        let ac = self.active_client.as_ref().unwrap();
        let new_port = ac.as_client().register_port(format!("{}_i_audio_l", name).as_str(), jack::AudioIn::default()).unwrap();
        input_ports.push(new_port);
        input_ports.push(self.active_client.as_ref().unwrap().as_client().register_port(format!("{}_i_audio_r", name).as_str(), jack::AudioIn::default()).unwrap());

        let mut output_ports: Vec<jack::Port<jack::AudioOut>> = Vec::new();
        output_ports.push(self.active_client.as_ref().unwrap().as_client().register_port(format!("{}_o_audio_l", name).as_str(), jack::AudioOut::default()).unwrap());
        output_ports.push(self.active_client.as_ref().unwrap().as_client().register_port(format!("{}_o_audio_r", name).as_str(), jack::AudioOut::default()).unwrap());

        self.active_client.as_ref().unwrap().as_client().connect_ports_by_name(
            format!("flydaw:{}_o_audio_l", name).as_str(),
            "flydaw:master_in_left"
        ).unwrap();

        self.active_client.as_ref().unwrap().as_client().connect_ports_by_name(
            format!("flydaw:{}_o_audio_r", name).as_str(),
            "flydaw:master_in_right"
        ).unwrap();



        self.audio_tracks.lock().unwrap().push(name.to_string());
        self.at_input_ports.lock().unwrap().insert(name.to_string(), input_ports);
        self.at_output_ports.lock().unwrap().insert(name.to_string(), output_ports);
        self.at_playback_queue.lock().unwrap().insert(name.to_string(), playback_queue);
        self.at_recordin_queue.lock().unwrap().insert(name.to_string(), recordin_queue);
        self.at_cur_frame.lock().unwrap().insert(name.to_string(), 0);
        self.at_control_states.lock().unwrap().insert(name.to_string(), control_states);
   /*         name: name.to_string(),
            input_ports,
            output_ports,
            playback_queue,
            recordin_queue,
            cur_frame: 0,
            control_states
        });*/

    }

    pub fn remove_track(
        &mut self, name: &str
    ) {

        if let Some(client) = &self.active_client {
            let jack_client = client.as_client();

            println!("{}_i_audio_l", name);
            jack_client.unregister_port(jack_client.port_by_name(
                format!("flydaw:{}_i_audio_l", name).as_str()
            ).unwrap()).unwrap();
            println!("{}_i_audio_r", name);
            jack_client.unregister_port(jack_client.port_by_name(
                format!("flydaw:{}_i_audio_r", name).as_str()
            ).unwrap()).unwrap();
            println!("{}_o_audio_l", name);
            jack_client.unregister_port(jack_client.port_by_name(
                format!("flydaw:{}_o_audio_l", name).as_str()
            ).unwrap()).unwrap();
            println!("{}_o_audio_r", name);
            jack_client.unregister_port(jack_client.port_by_name(
                format!("flydaw:{}_o_audio_r", name).as_str()
            ).unwrap()).unwrap();
        }
        let mut audio_tracks = self.audio_tracks.lock().unwrap();
        let mut rm_index = None;
        for i in 0..audio_tracks.len() {
            if audio_tracks[i] == name {
                rm_index = Some(i);
            }
        }
        if let Some(i) = rm_index {
            audio_tracks.remove(i);
        }
        drop(audio_tracks);

        self.at_input_ports.lock().unwrap().remove(name);
        self.at_output_ports.lock().unwrap().remove(name);
        self.at_playback_queue.lock().unwrap().remove(name);
        self.at_recordin_queue.lock().unwrap().remove(name);
        self.at_cur_frame.lock().unwrap().remove(name);
        self.at_control_states.lock().unwrap().remove(name);

        println!("audio track `{}` removed", name);
    }

    pub fn add_midi_track(
        &mut self, name: &str,
        playback_queue: Arc<Mutex<MidiPlaybackQueue>>,
        recordin_queue: Arc<Mutex<MidiRecordingQueue>>,
        control_states: Arc<Mutex<HashMap<String, bool>>>
    ) {
        let input_port: jack::Port<jack::MidiIn> =
            self.active_client.as_ref().unwrap().as_client().register_port(
                format!("{}_i_midi", name).as_str(),
                jack::MidiIn::default()
            ).unwrap();

        let output_port: jack::Port<jack::MidiOut> =
            self.active_client.as_ref().unwrap().as_client().register_port(
                format!("{}_o_midi", name).as_str(),
                jack::MidiOut::default()
            ).unwrap();

        self.midi_tracks.lock().unwrap().push(name.to_string());
        self.mt_input_ports.lock().unwrap().insert(name.to_string(), input_port);
        self.mt_output_ports.lock().unwrap().insert(name.to_string(), output_port);
        self.mt_playback_queue.lock().unwrap().insert(name.to_string(), playback_queue);
        self.mt_recordin_queue.lock().unwrap().insert(name.to_string(), recordin_queue);
        self.mt_control_states.lock().unwrap().insert(name.to_string(), control_states);

    }

    pub fn remove_midi_track(
        &mut self, name: &str
    ) {
        if let Some(client) = &self.active_client {
            let jack_client = client.as_client();

            println!("{}_i_audio_l", name);
            jack_client.unregister_port(jack_client.port_by_name(
                format!("flydaw:{}_i_midi", name).as_str()
            ).unwrap()).unwrap();
            println!("{}_o_audio_r", name);
            jack_client.unregister_port(jack_client.port_by_name(
                format!("flydaw:{}_o_midi", name).as_str()
            ).unwrap()).unwrap();
        }
        let mut midi_tracks = self.midi_tracks.lock().unwrap();
        let mut rm_index = None;
        for i in 0..midi_tracks.len() {
            if midi_tracks[i] == name {
                rm_index = Some(i);
            }
        }
        if let Some(i) = rm_index {
            midi_tracks.remove(i);
        }
        drop(midi_tracks);

        self.mt_input_ports.lock().unwrap().remove(name);
        self.mt_output_ports.lock().unwrap().remove(name);
        self.mt_playback_queue.lock().unwrap().remove(name);
        self.mt_recordin_queue.lock().unwrap().remove(name);
        self.mt_control_states.lock().unwrap().remove(name);

        println!("midi track `{}` removed", name);
    }

    pub fn deactivate(&mut self) {
        if let Some(client) = self.active_client.take() {
            client.deactivate().unwrap();
        }
    }

    pub fn sample_rate() -> usize {
        let (tmp_client, _status) = jack::Client::new("jack_sample_rate", jack::ClientOptions::NO_START_SERVER)
            .expect("Failed to open JACK client");
        let sample_rate = tmp_client.sample_rate();
        drop(tmp_client);
        sample_rate
    }


    pub fn buffer_size() -> usize {
        let (tmp_client, _status) = jack::Client::new("jack_buffer_size", jack::ClientOptions::NO_START_SERVER)
            .expect("Failed to open JACK client");
        let buffer_size = tmp_client.buffer_size();
        drop(tmp_client);
        buffer_size as usize
    }
}


struct Notifications;

impl jack::NotificationHandler for Notifications {
    fn thread_init(&self, _: &jack::Client) {
        println!("JACK: thread init");
    }

    fn shutdown(&mut self, status: jack::ClientStatus, reason: &str) {
        println!("JACK: shutdown with status {status:?} because \"{reason}\"",);
    }

    fn freewheel(&mut self, _: &jack::Client, is_enabled: bool) {
        println!(
            "JACK: freewheel mode is {}",
            if is_enabled { "on" } else { "off" }
        );
    }

    fn sample_rate(&mut self, _: &jack::Client, srate: jack::Frames) -> jack::Control {
        println!("JACK: sample rate changed to {srate}");
        jack::Control::Continue
    }

    fn client_registration(&mut self, _: &jack::Client, name: &str, is_reg: bool) {
        println!(
            "JACK: {} client with name \"{}\"",
            if is_reg { "registered" } else { "unregistered" },
            name
        );
    }

    fn port_registration(&mut self, _: &jack::Client, port_id: jack::PortId, is_reg: bool) {
        println!(
            "JACK: {} port with id {}",
            if is_reg { "registered" } else { "unregistered" },
            port_id
        );
    }

    fn port_rename(
        &mut self,
        _: &jack::Client,
        port_id: jack::PortId,
        old_name: &str,
        new_name: &str,
    ) -> jack::Control {
        println!("JACK: port with id {port_id} renamed from {old_name} to {new_name}",);
        jack::Control::Continue
    }

    fn ports_connected(
        &mut self,
        _: &jack::Client,
        port_id_a: jack::PortId,
        port_id_b: jack::PortId,
        are_connected: bool,
    ) {
        println!(
            "JACK: ports with id {} and {} are {}",
            port_id_a,
            port_id_b,
            if are_connected {
                "connected"
            } else {
                "disconnected"
            }
        );
    }

    fn graph_reorder(&mut self, _: &jack::Client) -> jack::Control {
        println!("JACK: graph reordered");
        jack::Control::Continue
    }

    fn xrun(&mut self, _: &jack::Client) -> jack::Control {
        println!("JACK: xrun occurred");
        jack::Control::Continue
    }
}
