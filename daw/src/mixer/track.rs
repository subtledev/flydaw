
use std::collections::HashMap;
use std::sync::{Arc, Mutex};

use crate::JackClient;


pub struct Track {
    pub name: String,
    pub control_states: Arc<Mutex<HashMap<String, bool>>>,
    pub jack_client: Arc<Mutex<JackClient>>
}


pub trait TrackTrait {
    fn new(jack_client: Arc<Mutex<JackClient>>, name: &String) -> Self;
}

impl TrackTrait for Track {

    fn new(jack_client: Arc<Mutex<JackClient>>, name: &String) -> Track {
        let control_states = Arc::new(Mutex::new(HashMap::new()));
        Track {
            name: name.clone(),
            control_states,
            jack_client
        }
    }

}

impl Track {

    pub fn set_control_state(&mut self, control_name: &str, state: bool) {
        self.control_states.lock().unwrap().insert(control_name.to_string(), state);
    }

    pub fn get_control_state(&mut self, control_name: &str) -> bool {
        *self.control_states.lock().unwrap().get(control_name).unwrap_or(&false)
    }
}


