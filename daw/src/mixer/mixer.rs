use std::sync::{Arc, Mutex};
use std::fs;
use std::path::Path;
use std::fs::File;
use std::io::{Read, Result};


use serde_json::json;


use crate::GUIProcess;
use crate::JackClient;
use crate::session::config::SessionConfig;
use crate::mixer::audio::track::AudioTrack;
use crate::mixer::audio::sample_cut::SampleCut;
use crate::mixer::midi::track::MidiTrack;
use crate::mixer::midi::sequence::MidiSequence;
use crate::mixer::track::TrackTrait;


pub struct Mixer {
  pub audio_tracks: Vec<AudioTrack>,
  midi_tracks: Vec<MidiTrack>,
  gui: Option<Arc<Mutex<GUIProcess>>>,
  session: Arc<Mutex<SessionConfig>>,
  jack_client: Arc<Mutex<JackClient>>,
}

impl Mixer {

    pub fn new(jack_client: Arc<Mutex<JackClient>>, session: Arc<Mutex<SessionConfig>>) -> Mixer {
        Mixer {
            audio_tracks: Vec::new(),
            midi_tracks: Vec::new(),
            gui: None,
            session,
            jack_client
        }
    }

    pub fn set_gui(&mut self, gui: Arc<Mutex<GUIProcess>>) {
        self.gui = Some(gui);
    }

    pub fn clear(&mut self) {
        for track in &mut self.audio_tracks {
            track.remove();
        }
        self.audio_tracks = Vec::new();

        for track in &mut self.midi_tracks {
            track.remove();
        }
        self.midi_tracks = Vec::new();

        let gui_arc = self.gui.as_ref().unwrap();
        let mut gui = gui_arc.lock().unwrap();
        gui.send(serde_json::json!({
            "command": "session",
            "unload": true
        }));
    }

    pub fn remove_audio_track(&mut self, name: String) {
        let mut rm_index = None;
        for i in 0..self.audio_tracks.len() {
            if (self.audio_tracks[i].track.name == name) {
                self.audio_tracks[i].remove();
                rm_index = Some(i);
            }
        }

        if let Some(i) = rm_index {
            self.audio_tracks.remove(i);
        }

        if let Some(gui_mutex) = self.gui.as_ref() {
            let mut gui = gui_mutex.lock().unwrap();
            gui.send(json!({
                "command": "tracks",
                "remove": {
                    "type": "audio",
                    "name": name
                }
            }));
        }
    }

    pub fn remove_midi_track(&mut self, name: String) {
        let mut rm_index = None;
        for i in 0..self.midi_tracks.len() {
            if (self.midi_tracks[i].track.name == name) {
                self.midi_tracks[i].remove();
                rm_index = Some(i);
            }
        }

        if let Some(i) = rm_index {
            self.midi_tracks.remove(i);
        }

        if let Some(gui_mutex) = self.gui.as_ref() {
            let mut gui = gui_mutex.lock().unwrap();
            gui.send(json!({
                "command": "tracks",
                "remove": {
                    "type": "midi",
                    "name": name
                }
            }));
        }
    }

    pub fn add_audio_track(&mut self, set_name: Option<String>) {

        let index = self.audio_tracks.len();
        let name = set_name.unwrap_or(
            format!("{}_audio_track", index)
        );

        println!("NEW AUDIO TRACK 1");

        let new_audio_track = AudioTrack::new(
            Arc::clone(&self.jack_client),
            &name
        );

        println!("NEW AUDIO TRACK 3");
        self.audio_tracks.push(new_audio_track);

        if let Some(gui_mutex) = self.gui.as_ref() {
            println!("GUI AVAILABLE");
            let mut gui = gui_mutex.lock().unwrap();
            println!("GUI LOCKED");
            println!("add_audio_track");
            gui.send(json!({
                "command": "tracks",
                "new_track": {
                    "type": "audio",
                    "name": name
                }
            }));
        }
    }

    pub fn add_midi_track(&mut self, set_name: Option<String>) {
        let index = self.midi_tracks.len();
        let name = set_name.unwrap_or(
            format!("{}_midi_track", index)
        );
        self.midi_tracks.push(MidiTrack::new(
            Arc::clone(&self.jack_client),
            &name
        ));

        if let Some(gui_mutex) = self.gui.as_ref() {
            let mut gui = gui_mutex.lock().unwrap();
            println!("add_audio_track");
            gui.send(json!({
                "command": "tracks",
                "new_track": {
                    "type": "midi",
                    "name": name
                }
            }));
        }
    }

    pub fn import_files(&mut self, track: String, files: Vec<String>, _position: f32) {
        // TODO: get and set position value of the drop
        
        let session_mutex = self.session.as_ref();
        let session = session_mutex.lock().unwrap();

        for file in files {
            println!("file:{}", file);
            match is_wav(file.as_str()) {
                Ok(true) => {
                    let src_path = Path::new(file.as_str());
                    let track_dir_path = Path::new(session.samples_dir_path.as_str()).join(track.clone());
                    fs::create_dir_all(track_dir_path.clone())
                        .unwrap_or_else(|error| eprintln!("Error creating directory: {}", error));
                    let destination_path = track_dir_path.join(
                        src_path.file_name().unwrap().to_str().unwrap()
                    );
                    println!("dst: {}", destination_path.to_str().unwrap());
                    if file != destination_path.clone().to_string_lossy().to_string() {
                        fs::copy(file, destination_path.clone())
                            .map_err(|err| eprintln!("Error: {}", err))
                            .and_then(|_| {
                                println!("File copied successfully.");
                                Ok(())
                            })
                            .unwrap_or_else(|_| ());
                    }

                    for etrack in &mut self.audio_tracks {
                        if etrack.track.name == track {
                            let new_sample_cut = SampleCut::new(
                                destination_path.to_str().unwrap().to_string(),
                                etrack.next_id,
                                Arc::clone(&self.session)
                            );

                            let new_sample_id = new_sample_cut.id;

                            let duration = new_sample_cut.duration;
                            etrack.add_sample_cut(new_sample_cut);
                            if let Some(gui_mutex) = self.gui.as_ref() {
                                let mut gui = gui_mutex.lock().unwrap();
                                gui.send(json!({
                                    "command": "tracks",
                                    "add_sample_cut": {
                                        "track": track,
                                        "full_path": destination_path,
                                        "duration": duration.as_secs_f64(),
                                        "position": 0,
                                        "id": new_sample_id
                                    }
                                }));
                            }

                        }
                    }
                },
                Ok(false) => println!("The file is not a WAV file."),
                Err(err) => eprintln!("Error reading WAV file: {}", err),
            }
        }


    }

    pub fn import_midi_files(&mut self, track: String, files: Vec<String>, position: f32) {
        let session_mut = self.session.as_ref().lock().unwrap();
        let samples_dir_path = session_mut.samples_dir_path.clone();
        drop(session_mut);

        for file in files {
            println!("file:{}", file);
            let src_path = Path::new(file.as_str());
            let track_dir_path = Path::new(samples_dir_path.as_str()).join(track.clone());
            fs::create_dir_all(track_dir_path.clone())
                .unwrap_or_else(|error| eprintln!("Error creating directory: {}", error));
            let destination_path = track_dir_path.join(
                src_path.file_name().unwrap().to_str().unwrap()
            );
            println!("dst: {}", destination_path.to_str().unwrap());
            if file != destination_path.clone().to_string_lossy().to_string() {
                fs::copy(file, destination_path.clone())
                    .map_err(|err| eprintln!("Error: {}", err))
                    .and_then(|_| {
                        println!("File copied successfully.");
                        Ok(())
                    })
                    .unwrap_or_else(|_| ());
            }

            for etrack in &mut self.midi_tracks {
                if etrack.track.name == track {
                    let new_seq = MidiSequence::new(
                        destination_path.to_str().unwrap().to_string(),
                        etrack.next_id,
                        Arc::clone(&self.session)
                    );

                    let new_sample_id = new_seq.id;

                    let duration = new_seq.duration;
                    etrack.add_sequence(new_seq);
                    if let Some(gui_mutex) = self.gui.as_ref() {
                        let mut gui = gui_mutex.lock().unwrap();
                        gui.send(json!({
                            "command": "tracks",
                            "add_sequence": {
                                "track": track,
                                "full_path": destination_path,
                                "duration": duration.as_secs_f64(),
                                "position": 0,
                                "id": new_sample_id
                            }
                        }));
                    }

                }
            }
        }

    }

    pub fn set_track_state(&mut self, track_name: String, which: String, state: bool) {
        for track in &mut self.audio_tracks {
            if track.track.name == track_name {
                track.set_control_state(which.as_str(), state);
                break;
            }
        }
        for track in &mut self.midi_tracks {
            if track.track.name == track_name {
                track.set_control_state(which.as_str(), state);
                break;
            }
        }
        if let Some(gui_mutex) = self.gui.as_ref() {
            let mut gui = gui_mutex.lock().unwrap();
            gui.send(json!({
                "command": "tracks",
                "set_state": {
                    "track": track_name,
                    "which": which,
                    "state": state
                }
            }));
        }
    }

    pub fn update(&mut self, position: usize) {
        for track in &mut self.audio_tracks {
            track.update(position);

            if track.get_control_state("record") {
                if track.is_recording {
                    track.record();
                } else {
                    let session_mut = self.session.as_ref().lock().unwrap();
                    let samples_dir_path = session_mut.samples_dir_path.clone();
                    drop(session_mut);

                    let (rec_id, rec_path) = track.start_recording(samples_dir_path);
                    if let Some(gui_mutex) = self.gui.as_ref() {
                        let mut gui = gui_mutex.lock().unwrap();
                        gui.send(json!({
                            "command": "tracks",
                            "recording": {
                                "track": track.track.name,
                                "sample": {
                                    "id": rec_id,
                                    "full_path": rec_path,
                                    "position": position
                                }
                            }
                        }));
                    }
                }
            }
        }

        for track in &mut self.midi_tracks {
            if track.get_control_state("record") {
                if !track.is_recording {
                    let session_mutex = self.session.as_ref();
                    let session = session_mutex.lock().unwrap();
                    let (rec_id, rec_path) = track.start_recording(session.samples_dir_path.clone());

                    if let Some(gui_mutex) = self.gui.as_ref() {
                        let mut gui = gui_mutex.lock().unwrap();
                        gui.send(json!({
                            "command": "tracks",
                            "recording": {
                                "track": track.track.name,
                                "sequence": {
                                    "id": rec_id,
                                    "full_path": rec_path,
                                    "position": position
                                }
                            }
                        }));
                    }
                }
            } else {
                track.update(position);
            }
        }
    }

    pub fn stop(&mut self, position: usize) {
        self.clear_queue();
        for track in &mut self.audio_tracks {
            if track.is_recording {
                track.stop_recording();
                if let Some(gui_mutex) = self.gui.as_ref() {
                    let mut gui = gui_mutex.lock().unwrap();
                    gui.send(json!({
                        "command": "tracks",
                        "stop_recording": {
                            "track": track.track.name,
                            "sample": {
                                "id": track.next_id - 1,
                                "position": position
                            }
                        }
                    }));
                }
            }
        }

        for track in &mut self.midi_tracks {
            if track.is_recording {
                track.stop_recording();
                if let Some(gui_mutex) = self.gui.as_ref() {
                    let mut gui = gui_mutex.lock().unwrap();
                    gui.send(json!({
                        "command": "tracks",
                        "stop_recording": {
                            "track": track.track.name,
                            "sequence": {
                                "id": track.next_id - 1,
                                "position": position
                            }
                        }
                    }));
                }
            }
        }
    }

    pub fn clear_queue(&mut self) {
        for track in &mut self.audio_tracks {
            track.clear_queue();
        }
    }

    pub fn remove_sample_cut(&mut self, track_name: String, cut_id: usize) {
        for track in &mut self.audio_tracks {
            if track.track.name == track_name {
                track.remove_sample_cut(cut_id);
                if let Some(gui_mutex) = self.gui.as_ref() {
                    let mut gui = gui_mutex.lock().unwrap();
                    gui.send(json!({
                        "command": "cut",
                        "remove": {
                            "track": track_name,
                            "id": cut_id
                        }
                    }));
                }
            }
        }
    }

    pub fn move_sample_cut(&mut self, track_name: String, cut_id: usize, position: usize) {
        for track in &mut self.audio_tracks {
            if track.track.name == track_name {
                track.move_sample_cut(cut_id, position);
                if let Some(gui_mutex) = self.gui.as_ref() {
                    let mut gui = gui_mutex.lock().unwrap();
                    gui.send(json!({
                        "command": "cut",
                        "move": {
                            "track": track_name,
                            "id": cut_id,
                            "position": position
                        }
                    }));
                }
            }
        }
    }


}

fn is_wav(file_path: &str) -> Result<bool> {
    // Read the first 12 bytes of the file
    let mut buffer = [0; 12];
    let mut file = File::open(file_path)?;

    file.read_exact(&mut buffer)?;

    // Check if the first 4 bytes match "RIFF" and the next 8 bytes match "WAVEfmt "
    Ok(&buffer[0..4] == b"RIFF" && &buffer[8..12] == b"WAVE")
}
