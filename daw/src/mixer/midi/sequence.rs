
use std::time::Duration;
use std::sync::{Arc, Mutex};

use midly::{Timing, num};

use crate::session::config::SessionConfig;

fn mid_duration(midi_file_path: &str) -> Duration {
    if std::fs::metadata(midi_file_path).is_ok() {
        let data = std::fs::read(midi_file_path).unwrap();
        let smf = midly::Smf::parse(&data).unwrap();

        let total_ticks: usize = smf
            .tracks
            .iter()
            .flat_map(|track| {
                let mut ticks = 0;
                track.iter().for_each(|event| {
                    ticks += event.delta.as_int() as usize;
                });
                vec![ticks]
            })
            .sum();

        // Calculate the duration in seconds based on the tempo information in the header
        let ticks_per_beat = match smf.header.timing {
            Timing::Metrical(tpb) => num::u15::as_int(tpb) as f64,
            Timing::Timecode(_, _) => {
                eprintln!("Unsupported MIDI time division type.");
                panic!("Unsupported MIDI time division type");
            }
        };

        // ticks_per_min = bpm * ppq
        // ticks_pes_sec = ticks_per_min / 60.0

        // Calculate the duration in seconds
        let duration_seconds = total_ticks as f64 / ((ticks_per_beat * 120.0) / 60.0);

        println!("MIDI SEQUENCE DURATION: {}s", duration_seconds);
        // Convert the duration to std::time::Duration
        Duration::from_secs_f64(duration_seconds)
    } else {
        Duration::from_secs_f64(0.0)
    }

}

pub struct MidiSequence {
    pub id: usize,
    pub full_path: String,
    pub duration: Duration,
    pub position: Duration,
    pub is_open: bool,
    session: Arc<Mutex<SessionConfig>>
}

impl MidiSequence {

    pub fn new(full_path: String, id: usize, session: Arc<Mutex<SessionConfig>>) -> MidiSequence {

        let duration: Duration = mid_duration(full_path.as_str());
        
        MidiSequence {
            id,
            full_path,
            duration: duration,
            position: Duration::from_secs(0),
            is_open: false,
            session
        }
    }

    pub fn open(&mut self) -> Vec<(u32, [u8; 3])> {
        self.is_open = true;
        let bytes = std::fs::read(self.full_path.clone()).unwrap();
        let smf = midly::Smf::parse(&bytes).unwrap();

        let mut queue = Vec::new();

        let mut total_frames = 0;

        for track in smf.tracks.iter() {
            for event in track {
                let mut raw_data: [u8; 3] = [0, 0, 0];
                let mut slice = &mut raw_data[0..3];
                event.kind
                    .as_live_event()
                    .expect("FAILED TO GET LIVE EVENT")
                    .write(&mut slice)
                    .expect("FAILED TO WRITE EVENT TO BYTES");
                let number_of_bytes = 3 - slice.len();

                let delta_frames = self.session.lock().unwrap().ticks_to_frames(
                    event.delta.as_int()
                );

                total_frames = total_frames + delta_frames;

                queue.push(
                    (
                        total_frames,
                        raw_data
                    )
                );
            }
        }


        return queue;
    }
}
