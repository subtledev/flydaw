use std::sync::{Arc, Mutex};
use std::path::Path;

use midly::live::LiveEvent;

use midly::num;

use crate::mixer::track::Track;
use crate::mixer::track::TrackTrait;
use crate::mixer::midi::sequence::MidiSequence;
use crate::JackClient;

use crate::jack::queue::midi::playback::MidiPlaybackQueue;
use crate::jack::queue::midi::recording::MidiRecordingQueue;

use crate::session::config::SessionConfig;

pub struct MidiTrack {
    pub track: Track,
    sequences: Vec<MidiSequence>,
    playback_queue: Arc<Mutex<MidiPlaybackQueue>>,
    recordin_queue: Arc<Mutex<MidiRecordingQueue>>,
    pub is_recording: bool,
    pub next_record_id: usize,
    pub next_id: usize,
    jack_client: Arc<Mutex<JackClient>>,
    session: Arc<Mutex<SessionConfig>>
}

impl TrackTrait for MidiTrack {

    fn new(jack_client: Arc<Mutex<JackClient>>, name: &String) -> MidiTrack {
        let playback_queue = Arc::new(Mutex::new(MidiPlaybackQueue::new()));
        let recordin_queue = Arc::new(Mutex::new(MidiRecordingQueue::new()));

        let track = Track::new(
            Arc::clone(&jack_client),
            name
        );

        let jc_clone =  Arc::clone(&jack_client);
        let jc_mutex = jc_clone.as_ref();
        let mut jc = jc_mutex.lock().unwrap();
        let session = Arc::clone(&jc.session);
        jc.add_midi_track(
            name,
            Arc::clone(&playback_queue),
            Arc::clone(&recordin_queue),
            Arc::clone(&track.control_states)
        );

        MidiTrack {
            track,
            sequences: Vec::new(),
            playback_queue,
            recordin_queue,
            is_recording: false,
            next_record_id: 0,
            next_id: 0,
            jack_client,
            session
        }
    }
}

impl MidiTrack {

    pub fn set_control_state(&mut self, control_name: &str, state: bool) {
        self.track.set_control_state(control_name, state);
    }

    pub fn get_control_state(&mut self, control_name: &str) -> bool {
        self.track.get_control_state(control_name)
    }


    pub fn add_sequence(&mut self, sequence: MidiSequence) {
        self.sequences.push(sequence);
        self.next_id += 1;
    }

    pub fn update(&mut self, position: usize) {
        let sample_rate = self.session.lock().unwrap().sample_rate as f64;

        for seq in &mut self.sequences {
            let start_position = (seq.position.as_secs_f64() * sample_rate) as usize;
            let end_position = start_position + (seq.duration.as_secs_f64() * sample_rate) as usize;

            if start_position < position && position < end_position {
                if !seq.is_open {
                    let mut playback_queue = self.playback_queue.lock().unwrap();
                    playback_queue.extend(seq.open());
                }
            } else {
                seq.is_open = false;
            }
        }

    }

    pub fn start_recording(&mut self, midi_dir_path: String) -> (usize, String) {
        println!("START MIDI RECORDING");
        self.is_recording = true;
        
        let file_path = Path::new(midi_dir_path.as_str())
            .join(self.track.name.clone())
            .join(format!("recording_{}.mid", self.next_record_id));
        self.next_record_id += 1;


        let new_sequence = MidiSequence::new(
            file_path.to_str().unwrap().to_string(),
            self.next_id,
            Arc::clone(&self.jack_client.lock().unwrap().session)
        );

        let nsc_id = new_sequence.id.clone();
        let nsc_full_path = new_sequence.full_path.clone();

        self.sequences.push(new_sequence);
        self.next_id += 1;

        (nsc_id, nsc_full_path)
    }

/*    pub fn record(&mut self) {

    }*/

    pub fn stop_recording(&mut self) {
        println!("STOP RECORDING");
        let mut session_mut = self.session.lock().unwrap();
        let sample_rate = session_mut.sample_rate as u32;
        let bpm = session_mut.bpm;
        let ticks_per_beat = session_mut.ticks_per_beat;
        drop(session_mut);

        let header = midly::Header {
            format: midly::Format::SingleTrack,
            timing: midly::Timing::Metrical(num::u15::new(ticks_per_beat as u16)),
        };
        let mut writer = midly::Smf::new(header);
        writer.tracks.push(Vec::new());
        let arena = midly::Arena::new();

        let mut last_delta_tick: u32 = 0;

        let mut recordin_queue = self.recordin_queue.lock().unwrap();


        while let Some((time, data)) = recordin_queue.next_event() {
//            let status_byte = data[0];
//            let channel = status_byte & 0x0F;

            let live_event = LiveEvent::parse(data.as_slice()).unwrap();

            let track_event_kind = live_event.as_track_event(&arena);

            let delta_in_ticks = ((time * ticks_per_beat as u32) as f32 / ((sample_rate * 60) as f32 / bpm)) as u32;
            let delta_after = delta_in_ticks.wrapping_sub(last_delta_tick);
            last_delta_tick = delta_in_ticks;
//            let delta_in_ticks = (time * ticks_per_beat) / (sample_rate * 60 / bpm);

            
            println!("{:?}", track_event_kind);
            let track_event = midly::TrackEvent {
                delta: delta_after.into(),//delta_after.into(),
                kind: track_event_kind
            };

            writer.tracks[0].push(track_event);
        }

        let save_seq = self.sequences.last().unwrap();
        println!("SAVE TO {}", save_seq.full_path.as_str());

        writer.save(save_seq.full_path.as_str()).expect("Failed to save MIDI file");
        println!("FILE SAVED");
        self.is_recording = false;
    }

    pub fn remove(&mut self) {
        self.track.jack_client.lock().unwrap().remove_midi_track(self.track.name.as_str());
    }
}
