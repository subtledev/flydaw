use std::path::Path;

use crate::mixer::track::Track;
use crate::mixer::track::TrackTrait;
use crate::mixer::audio::sample_cut::SampleCut;
use crate::JackClient;

use crate::jack::queue::audio::playback::AudioPlaybackQueue;
use crate::jack::queue::audio::recording::AudioRecordingQueue;

use std::time::Duration;

use std::sync::{Arc, Mutex};

use crate::session::config::SessionConfig;

use crate::globals;


pub struct AudioTrack {
    pub track: Track,
    pub sample_cuts: Vec<SampleCut>,
    playback_queue: Arc<Mutex<AudioPlaybackQueue>>,
    recording_queue: Arc<Mutex<AudioRecordingQueue>>,
    seek_distance: usize,
    pub seek_position: usize,
    pub next_id: usize,
    writer: Option<hound::WavWriter<std::io::BufWriter<std::fs::File>>>,
    pub next_record_id: usize,
    pub is_recording: bool,
    session: Arc<Mutex<SessionConfig>>
}

impl TrackTrait for AudioTrack {
    fn new(jack_client: Arc<Mutex<JackClient>>, name: &String) -> AudioTrack {
        println!("NEW AUDIO TRACK 2");

        let jc_mutex = jack_client.as_ref();
        let mut jc = jc_mutex.lock().unwrap();
        let session = Arc::clone(&jc.session);
        let audio_frame_size = jc.session.lock().unwrap().frame_size;

        let mut playback_queue = AudioPlaybackQueue::new();
        println!("set frame size");
        playback_queue.set_frame_size(audio_frame_size);
        let playback_queue_arc = Arc::new(Mutex::new(playback_queue));

        let mut recording_queue = AudioRecordingQueue::new();
        recording_queue.set_frame_size(audio_frame_size);
        let recording_queue_arc = Arc::new(Mutex::new(recording_queue));

        let track = Track::new(
            Arc::clone(&jack_client),
            name
        );

        jc.add_track(name, Arc::clone(&playback_queue_arc), Arc::clone(&recording_queue_arc), Arc::clone(&track.control_states));

        AudioTrack {
            track,
            sample_cuts: Vec::new(),
            playback_queue: playback_queue_arc,
            recording_queue: recording_queue_arc,
            seek_distance: 0,
            seek_position: 0,
            next_id: 0,
            writer: None,
            next_record_id: 0,
            is_recording: false,
            session
        }
    }
}

impl AudioTrack {

    pub fn set_control_state(&mut self, control_name: &str, state: bool) {
        self.track.set_control_state(control_name, state);
    }

    pub fn get_control_state(&mut self, control_name: &str) -> bool {
        self.track.get_control_state(control_name)
    }

    pub fn add_sample_cut(&mut self, sample_cut: SampleCut) {
        self.sample_cuts.push(sample_cut);
        self.next_id += 1;
    }

    pub fn update(&mut self, position: usize) {
        let mut session_mut = self.session.lock().unwrap();
        let sample_rate = session_mut.sample_rate as f64;
        let frame_size = session_mut.frame_size as usize;

        let mut loop_region: Option<(usize, usize)> = None;

        let mut seek_position = position + self.seek_distance;

        if let Some( (loop_start, loop_end) ) = session_mut.loop_region.as_ref() {
            loop_region = Some( (*loop_start, *loop_end) );
            if loop_end <= &seek_position {
                seek_position = seek_position - loop_end + loop_start;
            }
        }

        drop(session_mut);

        let mut no_frame: bool = true;

        for scut in &mut self.sample_cuts {

            let start_position = (scut.position.as_secs_f64() * sample_rate) as usize;
            let end_position = start_position + (scut.duration.as_secs_f64() * sample_rate) as usize;


            if start_position <= seek_position && seek_position <= end_position {
                no_frame = false;

                if self.seek_distance < globals::QUEUE_LENGTH * frame_size && self.seek_position != seek_position {

  

                    // TODO: Set queue limit to save memory with large samples
                    if !scut.src_open {
                        scut.open();
                    }

                      /*println!(
                            "SS:{} || SE:{} || TP:{} || SP:{}",
                            start_position,
                            end_position,
                            position,
                            seek_position
                        );*/

                    if let Some(frame) = scut.process(seek_position - start_position) {
                        let collected: Vec<Vec<f32>> = frame.iter().map(|inner_array| inner_array.to_vec()).collect();
                        let as_array: [Vec<f32>; 2] = collected.try_into().unwrap();


                        self.playback_queue.lock().unwrap().progress(as_array);

                    } else {
                        let as_array: [Vec<f32>; 2] = [vec![0.0; frame_size].clone(), vec![0.0; frame_size].clone()];
                        self.playback_queue.lock().unwrap().progress(as_array);
                        println!("Error processing audio at position {}", position);
                        // Handle the error as needed
                    }

                    self.seek_position = seek_position;
                    if self.seek_distance + frame_size < globals::QUEUE_LENGTH * frame_size {
                        self.seek_distance = self.seek_distance + frame_size;
                    }
                }

            } else {
                if scut.src_open {
                    scut.close();
                }
            }
         
        }

        if no_frame {
            let as_array: [Vec<f32>; 2] = [vec![0.0; frame_size].clone(), vec![0.0; frame_size].clone()];
            self.playback_queue.lock().unwrap().progress(as_array);
        }
    }

    pub fn start_recording(&mut self, samples_dir_path: String) -> (usize, String) {
        let mut session_mut = self.session.lock().unwrap();
        let sample_rate = session_mut.sample_rate as u32;
        drop(session_mut);

        let spec = hound::WavSpec {
            channels: 2 as u16,
            sample_rate: sample_rate,
            bits_per_sample: 16,
            sample_format: hound::SampleFormat::Int,
        };

        let track_dir_path = Path::new(samples_dir_path.as_str())
            .join(self.track.name.clone())
            .join(format!("recording_{}.wav", self.next_record_id));

        self.writer = Some(
            hound::WavWriter::create(track_dir_path.clone(), spec)
                .expect("Failed to create WAV writer")
        );

        let new_sample_cut = SampleCut::new(
            track_dir_path.to_str().unwrap().to_string(),
            self.next_id,
            Arc::clone(&self.session),
        );

        let nsc_id = new_sample_cut.id.clone();
        let nsc_full_path = new_sample_cut.full_path.clone();

        self.sample_cuts.push(new_sample_cut);
        self.next_id += 1;

        self.is_recording = true;
        self.next_record_id += 1;

        (nsc_id, nsc_full_path)
    }

    pub fn record(&mut self) {
        let mut recording_queue = self.recording_queue.lock().unwrap();
        recording_queue.write_next_frame(self.writer.as_mut().unwrap());
    }

    pub fn stop_recording(&mut self) {
        self.is_recording = false;
        self.writer.take().unwrap().finalize().expect("Failed to finalize WAV file");
    }

    pub fn clear_queue(&mut self) {
//        self.playback_queue.lock().unwrap().clear();
    }

    pub fn move_sample_cut(&mut self, cut_id: usize, position: usize) {
        let mut session_mut = self.session.lock().unwrap();
        let sample_rate = session_mut.sample_rate as f64;
        drop(session_mut);
        for scut in &mut self.sample_cuts {
            if scut.id == cut_id {
                scut.position = Duration::from_secs_f64(position as f64 * sample_rate);
            }
        }
    }

    pub fn remove_sample_cut(&mut self, cut_id: usize) {
        self.sample_cuts.retain(|scut| scut.id == cut_id);
    }

    pub fn remove(&mut self) {
        self.track.jack_client.lock().unwrap().remove_track(self.track.name.as_str());
    }
}
