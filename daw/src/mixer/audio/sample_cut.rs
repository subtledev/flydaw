use std::fs::File;
use std::io::BufReader;
use std::time::Duration;
use hound::{WavReader, Error};

use std::sync::{Arc, Mutex};

use crate::session::config::SessionConfig;

fn get_wav_duration(file_path: &str) -> Result<Duration, Error> {
    let reader = WavReader::open(file_path)?;
    let duration_in_samples = reader.duration();
    let sample_rate = reader.spec().sample_rate as u64;

    let duration_in_seconds = duration_in_samples as f64 / sample_rate as f64;

    Ok(Duration::from_secs_f64(duration_in_seconds))
}

fn resample_linear(
    input: &[f32],
    input_sample_rate: f64,
    output_sample_rate: f64,
) -> Result<Vec<f32>, &'static str> {
    let num_output_samples =
        ((input.len() as f64) * (output_sample_rate / input_sample_rate)).round() as usize;
    let mut output = Vec::with_capacity(num_output_samples);

    for i in 0..num_output_samples {
        let t = i as f64 * input_sample_rate / output_sample_rate;
        let index = t as usize;
        let frac = t - index as f64;

        let sample1 = if index < input.len() {
            input[index]
        } else {
            // Handle the case where the calculated index is out of bounds
            // For simplicity, we'll repeat the last available sample
            *input.last().ok_or("Input is empty")?
        };
        let sample2 = if index + 1 < input.len() {
            input[index + 1]
        } else {
            // Handle the case where the calculated index + 1 is out of bounds
            // For simplicity, we'll repeat the last available sample
            *input.last().ok_or("Input is empty")?
        };

        let interpolated_sample = sample1 + frac as f32 * (sample2 - sample1);
        output.push(interpolated_sample);
    }

    Ok(output)
}

pub struct SampleCut {
    pub id: usize,
    pub full_path: String,
    pub duration: Duration,
    pub position: Duration,
    pub src_open: bool,
    pub all_queued: bool,
    reader: Option<WavReader<BufReader<File>>>,
    session: Arc<Mutex<SessionConfig>>
}

impl SampleCut {
    pub fn new(full_path: String, id: usize, session: Arc<Mutex<SessionConfig>>) -> SampleCut {
        
        let duration: Duration = get_wav_duration(full_path.as_str())
            .unwrap_or_else(|err| {
                eprintln!("Error: {}", err);
                Duration::default() // Adjust the default value as needed
            });

        println!("WAW: {} duration: {}", full_path, duration.as_secs_f64());

        SampleCut {
            id,
            full_path,
            duration: duration,
            position: Duration::from_secs(0),
            src_open: false,
            reader: None,
            all_queued: false,
            session
        }
    }

    pub fn open(&mut self) {
        self.reader = Some(WavReader::open(self.full_path.as_str()).expect(
            format!("Failed to open WAV: {}", self.full_path).as_str()
        ));
        self.src_open = true;
    }

    pub fn reopen(&mut self) {
        println!("REOPEN! <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<");
        self.close();
        self.open();
    }

    pub fn process(&mut self, position: usize) -> Option<[Box<[f32]>; 2]> {
        if let Some(ref mut reader) = self.reader {
            let mut session_mut = self.session.lock().unwrap();
            let sample_rate = session_mut.sample_rate as f64;
            let frame_size = session_mut.frame_size;
            drop(session_mut);

            let source_sample_rate = reader.spec().sample_rate as f64;
            let ratio = sample_rate / source_sample_rate;

            let num_samples_needed = ((frame_size as f64) / ratio) as usize;

            let mut read_index: u32 = position as u32;

            // Seek to the specified position
            reader.seek(read_index).map_err(|e| {
                eprintln!("Error seeking: {}", e);
            }).ok()?;
            
            // Read the audio frame for left channel
            let mut left_frame: Vec<f32> = Vec::with_capacity(num_samples_needed);
            let mut right_frame: Vec<f32> = Vec::with_capacity(num_samples_needed);

            match reader.spec().bits_per_sample {
                16 => {
                    for _ in 0..num_samples_needed {
                        if let (Some(lsample), Some(rsample)) = (reader.samples::<i16>().next(), reader.samples::<i16>().next()) {
                            left_frame.push(lsample.unwrap() as f32 / i16::MAX as f32);
                            right_frame.push(rsample.unwrap() as f32 / i16::MAX as f32);
                        } else {
                            // End of file reached
                            eprintln!("EOF");
                            self.reopen();
                            self.all_queued = true;
                            return None;
                        }
                    }
                }
                24 => {
                    for _ in 0..num_samples_needed {
                        if let (Some(lsample), Some(rsample)) = (reader.samples::<i32>().next(), reader.samples::<i32>().next()) {
                            left_frame.push(lsample.unwrap() as f32 / i32::MAX as f32);
                            right_frame.push(rsample.unwrap() as f32 / i32::MAX as f32);
                        } else {
                            // End of file reached
                            eprintln!("EOF");
                            self.all_queued = true;
                            self.reopen();
                            return None;
                        }
                    }
                }
                32 => {
                    for _ in 0..num_samples_needed {
                        if let (Some(lsample), Some(rsample)) = (reader.samples::<i32>().next(), reader.samples::<i32>().next()) {
                            left_frame.push(lsample.unwrap() as f32 / i32::MAX as f32);
                            right_frame.push(rsample.unwrap() as f32 / i32::MAX as f32);
                        } else {
                            // End of file reached
                            eprintln!("EOF");
                            self.all_queued = true;
                            self.reopen();
                            return None;
                        }
                    }
                }
                _ => {
                    // Handle other bit depths if needed
                    eprintln!("Unsupported bit depth");
                    // You may want to add an error message or specific handling for unsupported bit depths
                }
            }



            // Perform sample rate conversion
            let left_frame_resampled =
                resample_linear(&left_frame, source_sample_rate, sample_rate).map_err(|e| {
                    eprintln!("Error resampling left frame: {}", e);
                }).ok()?;
            let right_frame_resampled =
                resample_linear(&right_frame, source_sample_rate, sample_rate).map_err(|e| {
                    eprintln!("Error resampling right frame: {}", e);
                }).ok()?;

            // Ensure that the frames have exactly `frame_size` samples
            let left_frame_resampled =
                left_frame_resampled[..frame_size].try_into().map_err(|e| {
                    eprintln!("Error converting left frame to array: {}", e);
                }).ok()?;
            let right_frame_resampled =
                right_frame_resampled[..frame_size].try_into().map_err(|e| {
                    eprintln!("Error converting right frame to array: {}", e);
                }).ok()?;

            Some([left_frame_resampled, right_frame_resampled])
        } else {
            println!("NO READER");
            None
        }
    }

    pub fn close(&mut self) {
        self.all_queued = false;
        self.src_open = false;
        self.reader = None;
    }
}
